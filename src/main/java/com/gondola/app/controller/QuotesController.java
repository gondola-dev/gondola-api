package com.gondola.app.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gondola.app.entities.NaicList;
import com.gondola.app.entities.QuotesInfo;
import com.gondola.app.model.QuotesReponseInfo;
import com.gondola.app.model.QuotesReqInfo;
import com.gondola.app.repository.GondolaRepository;
import com.gondola.app.service.NaicListService;
import com.gondola.app.transformer.GondolaRequestMapping;
import com.gondola.app.transformer.GondolaResTransformer;
import com.gondola.app.ws.response.model.LifeLink;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class QuotesController {

	private static final Logger logger = LoggerFactory.getLogger(QuotesController.class);

	@Autowired
	private GondolaRepository gondolaRepository;

	@Autowired
	private GondolaResTransformer gondolaResTransformer;

	@Autowired
	private GondolaRequestMapping gondolaRequestMapping;

	@Autowired
	private NaicListService naicListService;
	
	@PostMapping("/quotes")
	public ResponseEntity<QuotesReponseInfo> getQuotesInfo(@RequestBody QuotesReqInfo quotesReq, HttpSession session) {
		QuotesReponseInfo quoteRes = null;
		try {
			/*SubscriberResInfo userInfo = (SubscriberResInfo) session.getAttribute("SUBSCRIBER_INFO");
			if(null != userInfo) {
				logger.info("userIfnor" +userInfo.toString());
			}*/
			if(quotesReq.getSubscriberid() == 0 && quotesReq.isProd()){
				quoteRes = new QuotesReponseInfo();
				quoteRes.setStatus("FAILURE");
				quoteRes.setPageName("INVITE_PAGE");
				return new ResponseEntity<QuotesReponseInfo>(quoteRes, HttpStatus.OK);				
			}
			long startTime = System.currentTimeMillis();
			QuotesInfo quotesInfo  = gondolaResTransformer.mapQuotes(quotesReq);
			if(!quotesReq.isMaxprotectcall()){
				insertQuoteRequest(quotesInfo); 				
			}
			logger.info("get Life Link List initiated :: getLifeLinkList");
			List<LifeLink> lifeLinklist = gondolaRequestMapping.getLifeLinkList(quotesReq);
			if (null != lifeLinklist && !lifeLinklist.isEmpty()) {
				logger.info("filter the response with naic initiated : filterProductListWithNAIC");
				quoteRes = gondolaResTransformer.mapResponseWithProductList(lifeLinklist, fetchNaicList(), quotesInfo,"0");
			}
			long endTime = System.currentTimeMillis();
			logger.info("Total Time Taken for Response Return " + (endTime - startTime) + " milliseconds");
		} catch (Exception e) {
			logger.error("exception occurs in insert quotes method -- " + e);
			System.out.println("Exception in Save -- " + e);
		}
		return new ResponseEntity<QuotesReponseInfo>(quoteRes, HttpStatus.OK);
	}
	
	@PostMapping("/quotesnew")
	public ResponseEntity<QuotesReponseInfo> insertQuotesNew(@RequestBody QuotesReqInfo quotesReq) {
		QuotesReponseInfo quoteRes = null;
		try {
			long startTime = System.currentTimeMillis();
			QuotesInfo quotesInfo  = gondolaResTransformer.mapQuotes(quotesReq);
			//insertQuoteRequest(quotesInfo);
			logger.info("get Life Link List initiated :: getLifeLinkList");
			List<LifeLink> lifeLinklist = gondolaRequestMapping.getLifeLinkList(quotesReq);
			if (null != lifeLinklist && !lifeLinklist.isEmpty()) {
				logger.info("filter the response with naic initiated : filterProductListWithNAIC");
				quoteRes = gondolaResTransformer.mapResponseWithProductList(lifeLinklist, fetchNaicList(), quotesInfo, quotesReq.getWithNaic());
			}
			long endTime = System.currentTimeMillis();
			logger.info("Total Time Taken for Response Return " + (endTime - startTime) + " milliseconds");
		} catch (Exception e) {
			logger.error("exception occurs in insert quotes method -- " + e);
			System.out.println("Exception in Save -- " + e);
		}
		return new ResponseEntity<QuotesReponseInfo>(quoteRes, HttpStatus.OK);
	}

	@GetMapping("/getQuotes")
	public List<QuotesInfo> getQuotesInfo() {
		logger.info("get Quotes Info method initiated");
		List<QuotesInfo> quotesList = null;
		try {
			quotesList = gondolaRepository.findAll();
		} catch (Exception e) {
			logger.error("exception occurs in get Quotes Info method -- " + e);
			System.out.println("Exception in Save -- " + e);
		}
		logger.info("get Quotes Info method completed");
		return quotesList;
	}

	@GetMapping(path = "/naiclist")
	public List<NaicList> fetchNaicList() {
		logger.info("naic fetch method intialized");
		List<NaicList> naicList = null;
		try {
			naicList = naicListService.getNaicList();
		} catch (Exception ex) {
			logger.error("exception occurs in fetch naic method -- " + ex);
			System.out.println("Exception in fetchAll naic -- " + ex);
		}
		return naicList;
	}

	public QuotesInfo insertQuoteRequest(QuotesInfo quotesInfo) {
		QuotesInfo q = null;
		try {
			logger.info("insert quotes method initiated");
			q = gondolaRepository.save(quotesInfo);
			logger.info("insert quotes method completed");
		} catch (Exception e) {
			logger.error("exception occurs in insert QuoteRequest method -- " + e);
		}
		return q;
	}
}