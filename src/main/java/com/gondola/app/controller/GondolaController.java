package com.gondola.app.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gondola.app.entities.CompanyRatings;
import com.gondola.app.entities.HealthRatings;
import com.gondola.app.entities.InsurerList;
import com.gondola.app.entities.States;
import com.gondola.app.service.CompanyRatingsService;
import com.gondola.app.service.HealthRatingsService;
import com.gondola.app.service.InsurerListService;
import com.gondola.app.service.StateService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class GondolaController {

	private static Logger logger = LoggerFactory.getLogger(GondolaController.class);

	@Autowired
	private StateService stateService;

	@Autowired
	private HealthRatingsService healthRatingsService;

	@Autowired
	private CompanyRatingsService companyRatingsService;

	@Autowired
	private InsurerListService insurerListService;

	/*@Autowired
	private UserListService userListService;*/

	@GetMapping(path = "/states")
	public List<States> fetchStateList() {
		logger.info("state fetch method intialized");
		List<States> stateList = null;
		try {
			stateList = stateService.fetchStateList();
		} catch (Exception ex) {
			logger.error("exception occurs in fetch states method -- " + ex);
			System.out.println("Exception in fetchAll states -- " + ex);
		}
		return stateList;
	}

	@GetMapping(path = "/insurerlist")
	public List<InsurerList> fetchInsurerList() {
		logger.info("insurerlist fetch method intialized");
		List<InsurerList> insurerlist = null;
		try {
			insurerlist = insurerListService.fetchInsurerList();
		} catch (Exception ex) {
			logger.error("exception occurs in fetch insurerlist method -- " + ex);
		}
		return insurerlist;
	}

	@GetMapping(path = "/healthratings")
	public List<HealthRatings> fetchHealthRatings(@RequestParam(required = false, defaultValue = "1") Integer status) {
		logger.info("health ratings fetch method intialized");
		List<HealthRatings> healthRatings = null;
		try {
			healthRatings = healthRatingsService.fetchHealthRatingsList();
			if (null != healthRatings) {
				healthRatings = healthRatings.stream().filter(healthRating -> status == healthRating.getStatus())
						.collect(Collectors.toList());
			}
		} catch (Exception ex) {
			logger.error("exception occurs in fetch healthRatings method -- " + ex);
			System.out.println("Exception in fetchAll healthRatings -- " + ex);
		}
		return healthRatings;
	}

	@GetMapping(path = "/companyratings")
	public List<CompanyRatings> fetchCompanyRatings(HttpServletRequest request, HttpServletResponse response) {
		logger.info("company ratings fetch method intialized");
		logger.info("sess" + request.getSession().getAttribute("subscriberInfo"));
		List<CompanyRatings> companyRatings = null;
		try {
			companyRatings = companyRatingsService.fetchCompanyRatings();
		} catch (Exception ex) {
			logger.error("exception occurs in fetch companyratings method -- " + ex);
			System.out.println("Exception in fetchAll company ratings-- " + ex);
		}
		return companyRatings;
	}

	/*@PostMapping(path = "/insertuserlist")
	public String inserUserList(@RequestBody UserList ul) throws SQLException {
		logger.info("userList insert method intialized");
		String message = "";
		try {
			UserList userList = null;
			userList = userListService.insertUserList(ul);
			message = null != userList ? "success" : "failed";
		} catch (Exception ex) {
			message = "failed";
			logger.error("userList insert method -- " + ex);
		}
		return message;
	}

	@GetMapping(path = "/getenrolledusers")
	public List<UserList> fetchEnrolledUsers() throws SQLException {
		logger.info("userList fetch method intialized");
		List<UserList> userList = null;
		try {
			userList = userListService.fetchUserList();
		} catch (Exception ex) {
			logger.error("userList insert method -- " + ex);
		}
		return userList;
	}*/
}