package com.gondola.app.controller;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gondola.app.entities.SubscriberList;
import com.gondola.app.entities.UserInfoList;
import com.gondola.app.model.SubscriberResInfo;
import com.gondola.app.model.UserInfoReq;
import com.gondola.app.model.UserInfoRes;
import com.gondola.app.service.OtpService;
import com.gondola.app.service.SubscriberListService;
import com.gondola.app.service.UserInfoListService;
import com.gondola.app.transformer.GondolaReqTransformer;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class UserInfoController {

	private static Logger logger = LoggerFactory.getLogger(UserInfoController.class);
	
	private static Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");

	@Autowired
	private UserInfoListService userInfoListService;

	@Autowired
	private SubscriberListService subscriberListService;

	@Autowired
	private GondolaReqTransformer gondolaReqTransformer;

	@Autowired
	private OtpService otpService;

	@PostMapping(path = "/createuserinfo")
	public ResponseEntity<UserInfoRes> createUserInfo(@RequestBody UserInfoReq reqInfo) {
		logger.info("userList insert method intialized");
		UserInfoRes resInfo = null;
		boolean otpStatus = false;
		try {
			resInfo = new UserInfoRes();
			if(validate(reqInfo, resInfo, Boolean.TRUE)){
				return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
			}
			UserInfoList userInfo = userInfoListService.fetchUserInfoByEmail(reqInfo.getEmailaddress());
			if (null == userInfo) {
				SubscriberList subscriberInfo = subscriberListService.fetchSubscriberByEmail(reqInfo.getEmailaddress());
				if(null == subscriberInfo){
					subscriberInfo = gondolaReqTransformer.mapSubscriberInsertFromSignUp(reqInfo);
					SubscriberList slInsertInfo = subscriberListService.insertSubscriberList(subscriberInfo);
					if(null == slInsertInfo){
						resInfo.setStatus("ERROR_WHILE_CREATING_SUBSCRIBER");
						return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);						
					}
					otpStatus = otpService.generateOtp(reqInfo.getEmailaddress(), Boolean.FALSE);
					if (otpStatus) {
						resInfo.setStatus("SUCCESS");
						resInfo.setStatusCode("OTP_PAGE");
						return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);	
					} else {
						resInfo.setStatus("FAILED_TO_SEND_OTP");
						return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);	
					}				
				} else if(null != subscriberInfo && !"A".equals(subscriberInfo.getStatus())) {
					otpStatus = otpService.generateOtp(reqInfo.getEmailaddress(), Boolean.FALSE);
					if (otpStatus) {
						resInfo.setStatus("SUCCESS");
						resInfo.setStatusCode("OTP_PAGE");
						return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);	
					} else {
						resInfo.setStatus("FAILED_TO_SEND_OTP");
						return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);	
					}					
				}
				insertUserInfo(reqInfo, resInfo);
			} else {
				resInfo.setStatus("FAILURE");
				resInfo.setStatusCode("USER_EXIST");				
			}
		} catch (Exception ex) {
			logger.error("userList insert method -- " + ex);
			resInfo.setStatus("FAILURE");
			resInfo.setStatusCode("ERROR_CODE_999");
			return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
	}
	
	private void insertUserInfo(UserInfoReq reqInfo, UserInfoRes resInfo) {
		UserInfoList ul = reqMapping(reqInfo);
		UserInfoList userInfo = userInfoListService.insertUserList(ul);
		if(null != userInfo){
			resInfo.setStatus("SUCCESS");
			resInfo.setStatusCode("USER_CREATED");				
		} else {
			resInfo.setStatus("FAILURE");
			resInfo.setStatusCode("FAILED_TO_CREATE");					
		}
	}

	@PostMapping(path = "/getuserlist")
	public ResponseEntity<UserInfoRes> fetchEnrolledUsers(@RequestBody UserInfoReq reqInfo) {
		UserInfoRes resInfo = null;
		logger.info("userList fetch method intialized");
		UserInfoList userInfo = null;
		try {
			resInfo = new UserInfoRes();
			if (validate(reqInfo, resInfo, Boolean.FALSE)) {
				return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
			}
			userInfo = userInfoListService.fetchUserInfoByEmail(reqInfo.getEmailaddress());
			if (null != userInfo) {
				if (userInfo.getPassword().equals(reqInfo.getPassword())) {
					resInfo.setStatus("SUCCESS");
					resInfo.setStatusCode("EXISTS");
					resInfo.setEmailAddress(userInfo.getEmailaddress());
					resInfo.setFirstName(userInfo.getFirstname());
					resInfo.setMobileNumber(userInfo.getMobilenumber());
					resInfo.setSubscriberid(userInfo.getSubscriberid());
				} else {
					resInfo.setStatus("FAILURE");
					resInfo.setStatusCode("INCORRECT_PASSWORD");
				}
			} else {
				resInfo.setStatus("FAILURE");
				resInfo.setStatusCode("NOT_EXISTS");
			}
		} catch (Exception ex) {
			logger.error("userList insert method -- " + ex);
			resInfo.setStatus("FAILURE");
			resInfo.setStatusCode("ERROR_CODE_999");
			return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
	}

	public boolean validate(UserInfoReq reqInfo, UserInfoRes resInfo, boolean isSignUp) {
		boolean hasError = reqInfo.getEmailaddress().isEmpty();
		if(!hasError){
			Matcher matcher = emailPattern.matcher(reqInfo.getEmailaddress().trim());
			if(!matcher.matches()){
				resInfo.setStatus("FAILURE");
				resInfo.setStatusCode("EMAILID_INVALID");
				hasError = true;
			} else if(isSignUp && !reqInfo.getPassword().equals(reqInfo.getConfirmpassword())) {
				resInfo.setStatus("FAILURE");
				resInfo.setStatusCode("PASSWORD_NOT_MATCH");
				hasError = true;
			}
		} else {
			resInfo.setStatus("FAILURE");
			resInfo.setStatusCode("EMAILID_MANDATORY");
		}
		return hasError;
	}
	
	@PostMapping(path = "/validateSignInOTP")
	public ResponseEntity<UserInfoRes> validateOTP(@RequestBody UserInfoReq reqInfo, HttpServletRequest request) {
		UserInfoRes resInfo = new UserInfoRes();
		try {
			boolean validateStatus = otpService.validateOTP(reqInfo.getEmailaddress(),
					reqInfo.getOtpnumber());
			if (validateStatus) {
				SubscriberList subscriberInfo = subscriberListService
						.fetchSubscriberByEmail(reqInfo.getEmailaddress());
				subscriberInfo.setModifieddate(new Timestamp(System.currentTimeMillis()));
				subscriberInfo.setStatus("A");
				subscriberInfo = subscriberListService.insertSubscriberList(subscriberInfo);
				if(null != subscriberInfo){
					insertUserInfo(reqInfo, resInfo);
					return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
				} else {
					resInfo.setStatus("SUBSCRIBER_UPDATE_FAILURE");
					return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
				}
			} else {
				resInfo.setStatus("VERIFICATION_FAILURE");
				return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/resendSignInOTP")
	public ResponseEntity<SubscriberResInfo> resendOTPFromSignIn(@RequestBody UserInfoReq reqInfo) {
		boolean otpStatus = false;
		SubscriberResInfo resInfo = new SubscriberResInfo();
		try {
			SubscriberList subscriberInfo = subscriberListService
					.fetchSubscriberByEmail(reqInfo.getEmailaddress());
			if (null != subscriberInfo && "S".equals(subscriberInfo.getStatus())) {
				otpStatus = otpService.generateOtp(subscriberInfo.getEmailaddress(), Boolean.TRUE);
				if (otpStatus) {
					resInfo.setStatus("success");
					return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
				} else {
					resInfo.setStatus("failure");
					return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
				}
			} else {
				resInfo.setStatus("failure");
				return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path = "/forgetPassword")
	public ResponseEntity<UserInfoRes> forgetPassword(@RequestBody UserInfoReq reqInfo) {
		UserInfoRes resInfo = null;
		logger.info("forgetPassword method intialized");
		UserInfoList userInfo = null;
		try {
			resInfo = new UserInfoRes();
			if (validate(reqInfo, resInfo, Boolean.FALSE)) {
				return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
			}
			userInfo = userInfoListService.fetchUserInfoByEmail(reqInfo.getEmailaddress());
			if (null != userInfo) {
				resInfo.setStatus("SUCCESS");
				resInfo.setStatusCode("EXISTS");
			} else {
				resInfo.setStatus("FAILURE");
				resInfo.setStatusCode("NOT_EXISTS");
			}
		} catch (Exception ex) {
			logger.error("forgetPassword insert method -- " + ex);
			resInfo.setStatus("FAILURE");
			resInfo.setStatusCode("ERROR_CODE_999");
			return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
	}
	
	@PostMapping(path = "/resetPassword")
	public ResponseEntity<UserInfoRes> resetPassword(@RequestBody UserInfoReq reqInfo) {
		logger.info("reset Password method intialized");
		UserInfoRes resInfo = null;
		try {
			resInfo = new UserInfoRes();
			if(validate(reqInfo, resInfo, Boolean.TRUE)){
				return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
			}
			UserInfoList userInfo = userInfoListService.fetchUserInfoByEmail(reqInfo.getEmailaddress());
			if (null != userInfo) {
				userInfo.setPassword(reqInfo.getPassword());
				userInfo = userInfoListService.insertUserList(userInfo);
				if(null != userInfo){
					resInfo.setStatus("SUCCESS");
					resInfo.setStatusCode("PASSWORD_RESET_DONE");				
				} else {
					resInfo.setStatus("FAILURE");
					resInfo.setStatusCode("FAILED_TO_RESET");					
				}
			} else {
				resInfo.setStatus("FAILURE");
				resInfo.setStatusCode("NOT_EXISTS");				
			}
		} catch (Exception ex) {
			logger.error("userList insert method -- " + ex);
			resInfo.setStatus("FAILURE");
			resInfo.setStatusCode("ERROR_CODE_999");
			return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<UserInfoRes>(resInfo, HttpStatus.OK);
	}
	
	public UserInfoList reqMapping(UserInfoReq r) {
		UserInfoList ul = new UserInfoList();
		ul.setFirstname(r.getFirstname());
		ul.setEmailaddress(r.getEmailaddress());
		ul.setPassword(r.getPassword());
		ul.setSubscriberid(r.getSubscriberid());
		ul.setMobilenumber(r.getMobilenumber());
		ul.setIpaddress(r.getIpaddress());
		ul.setStatus(1);
		return ul;
	}
}