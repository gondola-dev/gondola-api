package com.gondola.app.controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gondola.app.entities.QuotesInfo;
import com.gondola.app.entities.StagingUserInfo;
import com.gondola.app.entities.SubscriberList;
import com.gondola.app.model.QuotesReqInfo;
import com.gondola.app.model.SubscriberReqInfo;
import com.gondola.app.model.SubscriberResInfo;
import com.gondola.app.service.GondolaService;
import com.gondola.app.service.OtpService;
import com.gondola.app.service.StagingUserInfoService;
import com.gondola.app.service.SubscriberListService;
import com.gondola.app.transformer.GondolaReqTransformer;
import com.gondola.app.validator.GondolaValidator;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class SubscriberController {

	private static Logger logger = LoggerFactory.getLogger(SubscriberController.class);

	@Autowired
	private GondolaReqTransformer gondolaReqTransformer;

	@Autowired
	private OtpService otpService;

	@Autowired
	private SubscriberListService subscriberListService;
	
	@Autowired
	private StagingUserInfoService stagingUserInfoService;

	@Autowired
	private GondolaService gondolaService;

	@Autowired
	private GondolaValidator gondolaValidator;


	@GetMapping(path = "/fetchUserInfo")
	public ResponseEntity<SubscriberResInfo> fetchUsers(HttpServletRequest request, @RequestParam String ipaddress) throws SQLException {
		logger.info("fetchUsers method intialized");
		SubscriberResInfo resInfo = null;
		try {
			/*resInfo = (SubscriberResInfo) request.getSession().getAttribute("SUBSCRIBER_INFO");
			if(resInfo != null){
				resInfo.setQuoteList(mapQuotesResponseList(ipaddress));
				request.getSession().setAttribute("SUBSCRIBER_INFO", resInfo);
			} else {*/
				SubscriberList subscriberInfo = subscriberListService.fetchSubscriberByIpaddress(ipaddress);
				resInfo = new SubscriberResInfo();
				if (null != subscriberInfo) {
					resInfo.setPageName("homePage");
					resInfo.setEmailAddress(subscriberInfo.getEmailaddress());
					resInfo.setFirstName(subscriberInfo.getFirstname());
					resInfo.setMobileNumber(subscriberInfo.getMobilenumber());
					resInfo.setSubscriberid(subscriberInfo.getId());
					resInfo.setIpaddress(ipaddress);
					request.getSession().setAttribute("userInfo", resInfo);
					resInfo.setQuoteList(mapQuotesResponseList(ipaddress));
					//request.getSession().setAttribute("SUBSCRIBER_INFO", (SubscriberResInfo)resInfo);
				} else {
					resInfo.setPageName("invitePage");
				}
			//}
			
		} catch (Exception ex) {
			logger.error("Exception in fetchUsers method -- " + ex);
			return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
	}
	
	@GetMapping(path = "/fetchStagingInfo")
	public ResponseEntity<SubscriberResInfo> fetchStagingInfo(HttpServletRequest request, @RequestParam String ipaddress) throws SQLException {
		logger.info("fetchUsers method intialized");
		SubscriberResInfo resInfo = new SubscriberResInfo();
		try {
			/*resInfo = (SubscriberResInfo) request.getSession().getAttribute("SUBSCRIBER_INFO");
			if(resInfo != null){
				resInfo.setQuoteList(mapQuotesResponseList(ipaddress));
				request.getSession().setAttribute("SUBSCRIBER_INFO", resInfo);
			} else {*/
				resInfo = new SubscriberResInfo();
				StagingUserInfo stagingUserInfo = stagingUserInfoService.fetchStagingUserInfoByIpaddress(ipaddress);
				if (null != stagingUserInfo) {
					resInfo.setPageName("homePage");
					SubscriberList subscriberInfo = subscriberListService.fetchSubscriberByIpaddress(ipaddress);
					if(null != subscriberInfo) {
						resInfo.setEmailAddress(subscriberInfo.getEmailaddress());
						resInfo.setFirstName(subscriberInfo.getFirstname());
						resInfo.setMobileNumber(subscriberInfo.getMobilenumber());
						resInfo.setSubscriberid(subscriberInfo.getId());
						resInfo.setIpaddress(ipaddress);
						resInfo.setDataExist(true);
						resInfo.setQuoteList(mapQuotesResponseList(ipaddress));
						//request.getSession().setAttribute("SUBSCRIBER_INFO", resInfo);
					}
				} else {
					resInfo.setPageName("popupPage");
				}
			//}
		} catch (Exception ex) {
			logger.error("Exception in fetchUsers method -- " + ex);
			return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
	}
	


	@GetMapping(path = "/insertStagingUser")
	public boolean insertStagingUserInfo(@RequestParam String ipaddress, HttpServletRequest request) {
		SubscriberResInfo resInfo = new SubscriberResInfo();
		try {
			StagingUserInfo stagingUserInfo = stagingUserInfoService.fetchStagingUserInfoByIpaddress(ipaddress);
			if (null == stagingUserInfo) {
				stagingUserInfo = new StagingUserInfo();
				stagingUserInfo.setIpaddress(ipaddress);
				stagingUserInfo = stagingUserInfoService.insertStagingUserInfo(stagingUserInfo);
				return null != stagingUserInfo ? true : false;
			}else {
				return false;			
			}
		} catch (Exception ex) {
			logger.error("createSubscriberInfo method -- " + ex);
			resInfo.setStatus("failure");
			return false;
		}
	}

	@PostMapping(path = "/createsubscriber")
	public ResponseEntity<SubscriberResInfo> createSubscriberInfo(@RequestBody SubscriberReqInfo subscriberReqInfo, HttpServletRequest request) {
		boolean otpStatus = false;
		SubscriberResInfo resInfo = new SubscriberResInfo();
		List<String> errorList = new ArrayList<>();
		try {
			if(subscriberReqInfo.getIpaddress().isEmpty()){
				resInfo.setStatus("FAILURE");
				resInfo.setPageName("IPADDRESS_EMPTY_POPUP");
				return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
			}
			gondolaValidator.validate(errorList, subscriberReqInfo);
			if (errorList.isEmpty()) {
				SubscriberList subscriberInfo = subscriberListService
						.fetchSubscriberByEmail(subscriberReqInfo.getEmail_address());
				if (null == subscriberInfo) {
					subscriberInfo = gondolaReqTransformer.mapSubscriberInsert(subscriberReqInfo);
					SubscriberList slInsertInfo = subscriberListService.insertSubscriberList(subscriberInfo);
					if (null != slInsertInfo) {
						otpStatus = otpService.generateOtp(slInsertInfo.getEmailaddress(), Boolean.FALSE);
						resInfo.setPageName("otpPage");
					}
				} else if (!"A".equals(subscriberInfo.getStatus())) {
					otpStatus = otpService.generateOtp(subscriberInfo.getEmailaddress(), Boolean.FALSE);
					resInfo.setPageName("otpPage");
				} else {
					if (!subscriberReqInfo.getFirstname().equalsIgnoreCase(subscriberInfo.getFirstname())) {
						subscriberInfo.setFirstname(subscriberReqInfo.getFirstname());
						resInfo.setFirstName(subscriberInfo.getFirstname());
					}
					subscriberInfo.setIpaddress(subscriberReqInfo.getIpaddress());
					subscriberInfo = subscriberListService.insertSubscriberList(subscriberInfo);
					resInfo.setPageName("homePage");
					resInfo.setStatus("success");
					return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
				}
				if (otpStatus) {
					resInfo.setStatus("success");
					return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
				} else {
					resInfo.setStatus("failure");
					return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
				}
			} else {
				resInfo.setErrorList(errorList);
				resInfo.setStatus("failure");
				return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
			}
		} catch (Exception ex) {
			logger.error("createSubscriberInfo method -- " + ex);
			resInfo.setStatus("failure");
			return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/validateOTP")
	public ResponseEntity<SubscriberResInfo> validateOTP(@RequestBody SubscriberReqInfo subscriberReqInfo, HttpServletRequest request) {
		SubscriberResInfo resInfo = new SubscriberResInfo();
		try {
			boolean validateStatus = otpService.validateOTP(subscriberReqInfo.getEmail_address(),
					subscriberReqInfo.getOtpnumber());
			if (validateStatus) {
				SubscriberList subscriberInfo = subscriberListService
						.fetchSubscriberByEmail(subscriberReqInfo.getEmail_address());
				subscriberInfo.setModifieddate(new Timestamp(System.currentTimeMillis()));
				subscriberInfo.setStatus("A");
				subscriberListService.insertSubscriberList(subscriberInfo);
				resInfo.setStatus("success");
				return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
			} else {
				resInfo.setStatus("failure");
				return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/resendOTP")
	public ResponseEntity<SubscriberResInfo> resendOTP(@RequestBody SubscriberReqInfo subscriberReqInfo) {
		boolean otpStatus = false;
		SubscriberResInfo resInfo = new SubscriberResInfo();
		try {
			SubscriberList subscriberInfo = subscriberListService
					.fetchSubscriberByEmail(subscriberReqInfo.getEmail_address());
			if (null != subscriberInfo && "S".equals(subscriberInfo.getStatus())) {
				otpStatus = otpService.generateOtp(subscriberInfo.getEmailaddress(), Boolean.TRUE);
				if (otpStatus) {
					resInfo.setStatus("success");
					return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
				} else {
					resInfo.setStatus("failure");
					return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
				}
			} else {
				resInfo.setStatus("failure");
				return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<SubscriberResInfo>(resInfo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<QuotesReqInfo> mapQuotesResponseList(String ipaddress){
		List<QuotesInfo> quoteList = gondolaService.fetchQuotesInfoByIpaddress(ipaddress);
		Set<QuotesReqInfo> quoteInfoList = new HashSet<>();
		if (!quoteList.isEmpty()) {
			quoteList = quoteList.stream().filter(c -> Objects.nonNull(c.getCreatedinfo())).sorted(Comparator.comparing(QuotesInfo::getCreatedinfo).reversed()).collect(Collectors.toList());
			for(QuotesInfo q : quoteList) {
				QuotesReqInfo res = new QuotesReqInfo();
				res.setAge(q.getAge());
				res.setAmount(q.getAmount());
				res.setAnnualincome(q.getAnnualincome());
				res.setEstAssets(q.getEstAssets());
				res.setExpectedretirementage(q.getExpectedretirementage());
				res.setYears(q.getYears());
				res.setGender(q.getGender());
				res.setIssmoker(q.getIssmoker());
				res.setIsWorking(q.getIsWorking());
				res.setStatecode(q.getStatecode());
				quoteInfoList.add(res);
			}
			quoteInfoList = quoteInfoList.stream().filter(Objects::nonNull).distinct().limit(3).collect(Collectors.toSet());
		}
		return new ArrayList<QuotesReqInfo>(quoteInfoList);
	}
}