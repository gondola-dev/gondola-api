package com.gondola.app.ws.request.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
	    "faceAmount",
	    "paymentMode",
	    "underwritingClassInfo",
	    "riders"
	})
public class Scenario {
	 private long faceAmount;
	 private String paymentMode;
	 private List<UnderwritingClassInfo> underwritingClassInfo;
	 private List<Riders> riders;
	 
	 @XmlElement(name = "FaceAmount")
	 public long getFaceAmount() {
		return faceAmount;
	}
	public void setFaceAmount(long faceAmount) {
		this.faceAmount = faceAmount;
	}
	 @XmlElement(name = "PaymentMode")
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	@XmlElement(name = "UnderwritingClassInfo")
	public List<UnderwritingClassInfo> getUnderwritingClassInfo() {
		return underwritingClassInfo;
	}
	public void setUnderwritingClassInfo(List<UnderwritingClassInfo> underwritingClassInfo) {
		this.underwritingClassInfo = underwritingClassInfo;
	}
	@XmlElement(name = "Riders")
	public List<Riders> getRiders() {
		return riders;
	}
	public void setRider(List<Riders> riders) {
		this.riders = riders;
	}
	
	 
}
