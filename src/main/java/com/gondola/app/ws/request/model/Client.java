package com.gondola.app.ws.request.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlRootElement
@XmlType(propOrder = {
	    "applicant",
	    "stateOfIssue"
	})
public class Client {
	private List<Applicant> applicant;
	private String stateOfIssue;
	
	@XmlElement(name = "Applicant")
	public List<Applicant> getApplicant() {
		return applicant;
	}
	public void setApplicant(List<Applicant> applicant) {
		this.applicant = applicant;
	}
	@XmlElement(name = "StateOfIssue")
	public String getStateOfIssue() {
		return stateOfIssue;
	}
	public void setStateOfIssue(String stateOfIssue) {
		this.stateOfIssue = stateOfIssue;
	}
	
	
}
