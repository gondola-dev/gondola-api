package com.gondola.app.ws.request.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "ValueTypeAttribute")
	public class ValueTypeAttribute {

	    @XmlAttribute(name = "value", required = true)
	    protected String value;
	    @XmlAttribute(name = "type", required = true)
	    protected String type;
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
	    
	    
}
