package com.gondola.app.ws.request.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "LifeLink")
@XmlType(propOrder = {
	    "ll",
	    "userInformation",
	    "client",
	    "vitalTermNet"
	})
public class LifeLink {
	private String xmlns = "urn:lifelink-schema";
	private List<LL> ll;
	private List<UserInformation> userInformation;
	private List<Client> client;
	private List<VitalTermNet> vitalTermNet;
	
	@XmlElement(name = "LL")
	public List<LL> getLl() {
		return ll;
	}
	public void setLl(List<LL> ll) {
		this.ll = ll;
	}
	@XmlElement(name = "UserInformation")
	public List<UserInformation> getUserInformation() {
		return userInformation;
	}
	public void setUserInformation(List<UserInformation> userInformation) {
		this.userInformation = userInformation;
	}
	@XmlElement(name = "Client")
	public List<Client> getClient() {
		return client;
	}
	public void setClient(List<Client> client) {
		this.client = client;
	}
	@XmlElement(name = "VitalTermNet")
	public List<VitalTermNet> getVitalTermNet() {
		return vitalTermNet;
	}
	public void setVitalTermNet(List<VitalTermNet> vitalTermNet) {
		this.vitalTermNet = vitalTermNet;
	}
	@XmlAttribute(name = "xmlns")
	public String getXmlns() {
		return xmlns;
	}
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}
	
    
}
