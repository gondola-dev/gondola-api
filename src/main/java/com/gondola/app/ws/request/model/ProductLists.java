package com.gondola.app.ws.request.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
	    "yearsLevel",
	    "productType"
	})
public class ProductLists {
	 private int yearsLevel;
	 private String productType;
	 
	@XmlElement(name = "YearsLevel")
	public int getYearsLevel() {
		return yearsLevel;
	}
	public void setYearsLevel(int yearsLevel) {
		this.yearsLevel = yearsLevel;
	}
	@XmlElement(name = "ProductType")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	 
	 
}
