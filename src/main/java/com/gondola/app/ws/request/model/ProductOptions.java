package com.gondola.app.ws.request.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
	    "breakDownPremiums",
	    "calcToPenny",
	    "displayProductClasses",
	    "getCompanyInfo",
	    "showCurrentRates",
	    "showRemovedProducts",
	    "showProdInfoOnRemoval",
	    "showProdInfoOnly",
	    "showPremiumsOnly",
	    "calc1stYearModal",
	    "showAvailableRiders",
	})
public class ProductOptions {
	 private int breakDownPremiums;
	 private int calcToPenny;
	 private int displayProductClasses;
	 private int getCompanyInfo;
	 private int showCurrentRates;
	 private int showRemovedProducts;
	 private int showProdInfoOnRemoval;
	 private int showProdInfoOnly;
	 private int showPremiumsOnly;
	 private int calc1stYearModal;
	 private int showAvailableRiders;
	 
	 @XmlElement(name = "BreakDownPremiums")
	public int getBreakDownPremiums() {
		return breakDownPremiums;
	}
	public void setBreakDownPremiums(int breakDownPremiums) {
		this.breakDownPremiums = breakDownPremiums;
	}
	@XmlElement(name = "CalcToPenny")
	public int getCalcToPenny() {
		return calcToPenny;
	}
	public void setCalcToPenny(int calcToPenny) {
		this.calcToPenny = calcToPenny;
	}
	@XmlElement(name = "DisplayProductClasses")
	public int getDisplayProductClasses() {
		return displayProductClasses;
	}
	public void setDisplayProductClasses(int displayProductClasses) {
		this.displayProductClasses = displayProductClasses;
	}
	@XmlElement(name = "GetCompanyInfo")
	public int getGetCompanyInfo() {
		return getCompanyInfo;
	}
	public void setGetCompanyInfo(int getCompanyInfo) {
		this.getCompanyInfo = getCompanyInfo;
	}
	@XmlElement(name = "ShowCurrentRates")
	public int getShowCurrentRates() {
		return showCurrentRates;
	}
	public void setShowCurrentRates(int showCurrentRates) {
		this.showCurrentRates = showCurrentRates;
	}
	@XmlElement(name = "ShowRemovedProducts")
	public int getShowRemovedProducts() {
		return showRemovedProducts;
	}
	public void setShowRemovedProducts(int showRemovedProducts) {
		this.showRemovedProducts = showRemovedProducts;
	}
	@XmlElement(name = "ShowProdInfoOnRemoval")
	public int getShowProdInfoOnRemoval() {
		return showProdInfoOnRemoval;
	}
	public void setShowProdInfoOnRemoval(int showProdInfoOnRemoval) {
		this.showProdInfoOnRemoval = showProdInfoOnRemoval;
	}
	@XmlElement(name = "ShowProdInfoOnly")
	public int getShowProdInfoOnly() {
		return showProdInfoOnly;
	}
	public void setShowProdInfoOnly(int showProdInfoOnly) {
		this.showProdInfoOnly = showProdInfoOnly;
	}
	@XmlElement(name = "ShowPremiumsOnly")
	public int getShowPremiumsOnly() {
		return showPremiumsOnly;
	}
	public void setShowPremiumsOnly(int showPremiumsOnly) {
		this.showPremiumsOnly = showPremiumsOnly;
	}
	@XmlElement(name = "Calc1stYearModal")
	public int getCalc1stYearModal() {
		return calc1stYearModal;
	}
	public void setCalc1stYearModal(int calc1stYearModal) {
		this.calc1stYearModal = calc1stYearModal;
	}
	@XmlElement(name = "ShowAvailableRiders")
	public int getShowAvailableRiders() {
		return showAvailableRiders;
	}
	public void setShowAvailableRiders(int showAvailableRiders) {
		this.showAvailableRiders = showAvailableRiders;
	}
	
	 
}
