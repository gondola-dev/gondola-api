package com.gondola.app.ws.request.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rider", propOrder = {
    "riderRequestInfo"
})
public class Rider {
	 @XmlElement(name = "RiderRequestInfo")
	    protected RiderRequestInfo riderRequestInfo;
	    @XmlAttribute(name = "name")
	    protected String name;
	    @XmlAttribute(name = "id")
	    protected Integer id;
		public RiderRequestInfo getRiderRequestInfo() {
			return riderRequestInfo;
		}
		public void setRiderRequestInfo(RiderRequestInfo riderRequestInfo) {
			this.riderRequestInfo = riderRequestInfo;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		} 
	 
}
