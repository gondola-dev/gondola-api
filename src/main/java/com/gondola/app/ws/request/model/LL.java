package com.gondola.app.ws.request.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlRootElement
@XmlType(propOrder = {
	    "userName",
	    "userPassword",
	    "interfaceType",
	    "outputType",
	    "logout",
	    "tool"
	})
public class LL {

	 private String userName;
	 private String userPassword;
	 private String interfaceType;
	 private String outputType;
	 private String logout;
	 private List<Tool> tool;
	 
	 @XmlElement(name = "UserName")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@XmlElement(name = "UserPassword")
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	@XmlElement(name = "InterfaceType")
	public String getInterfaceType() {
		return interfaceType;
	}
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}
	@XmlElement(name = "OutputType")
	public String getOutputType() {
		return outputType;
	}
	public void setOutputType(String outputType) {
		this.outputType = outputType;
	}
	@XmlElement(name = "Logout")
	public String getLogout() {
		return logout;
	}
	public void setLogout(String logout) {
		this.logout = logout;
	}
	@XmlElement(name = "Tool")
	public List<Tool> getTool() {
		return tool;
	}
	public void setTool(List<Tool> tool) {
		this.tool = tool;
	}
	 
	 
}
