package com.gondola.app.ws.request.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
	    "uClass",
	    "smokingStatus",
	})
public class UnderwritingClassInfo {
     private String type = "VT";
	 private int uClass;
	 private String smokingStatus;
	 
	@XmlElement(name = "Class")
	public int getuClass() {
		return uClass;
	}
	public void setuClass(int uClass) {
		this.uClass = uClass;
	}
	@XmlElement(name = "SmokingStatus")
	public String getSmokingStatus() {
		return smokingStatus;
	}
	public void setSmokingStatus(String smokingStatus) {
		this.smokingStatus = smokingStatus;
	}
	@XmlAttribute(name = "type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	 
	 
	 
}
