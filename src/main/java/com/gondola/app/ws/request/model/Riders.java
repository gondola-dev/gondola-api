package com.gondola.app.ws.request.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
	    "rider",
	})
public class Riders {
	 private List<Rider> rider;

	 @XmlElement(name = "Rider")
	public List<Rider> getRider() {
		return rider;
	}

	public void setRider(List<Rider> rider) {
		this.rider = rider;
	}
	 
	 
	 
}
