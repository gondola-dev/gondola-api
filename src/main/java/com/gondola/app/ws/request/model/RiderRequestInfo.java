package com.gondola.app.ws.request.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiderRequestInfo", propOrder = {
    "unitAmount",
    "useMaxUnits",
    "childAgeYoungest",
    "adbFaceAmount",
    "useADBMaxFace"
})
public class RiderRequestInfo {
	@XmlElement(name = "UnitAmount")
	protected Double unitAmount;
    @XmlElement(name = "UseMaxUnits")
    protected Boolean useMaxUnits;
    @XmlElement(name = "ChildAgeYoungest")
    protected ValueTypeAttribute childAgeYoungest;
    @XmlElement(name = "ADBFaceAmount")
    protected Long adbFaceAmount;
    @XmlElement(name = "UseADBMaxFace")
    protected Boolean useADBMaxFace;
	public Double getUnitAmount() {
		return unitAmount;
	}
	public void setUnitAmount(Double unitAmount) {
		this.unitAmount = unitAmount;
	}
	public Boolean getUseMaxUnits() {
		return useMaxUnits;
	}
	public void setUseMaxUnits(Boolean useMaxUnits) {
		this.useMaxUnits = useMaxUnits;
	}
	public ValueTypeAttribute getChildAgeYoungest() {
		return childAgeYoungest;
	}
	public void setChildAgeYoungest(ValueTypeAttribute childAgeYoungest) {
		this.childAgeYoungest = childAgeYoungest;
	}
	public Long getAdbFaceAmount() {
		return adbFaceAmount;
	}
	public void setAdbFaceAmount(Long adbFaceAmount) {
		this.adbFaceAmount = adbFaceAmount;
	}
	public Boolean getUseADBMaxFace() {
		return useADBMaxFace;
	}
	public void setUseADBMaxFace(Boolean useADBMaxFace) {
		this.useADBMaxFace = useADBMaxFace;
	}
    
	
}
