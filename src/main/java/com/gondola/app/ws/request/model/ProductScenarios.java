package com.gondola.app.ws.request.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
	    "scenario"
	})
public class ProductScenarios {
	private List<Scenario> scenario;
    
	@XmlElement(name = "Scenario")
	public List<Scenario> getScenario() {
		return scenario;
	}

	public void setScenario(List<Scenario> scenario) {
		this.scenario = scenario;
	}
	
}
