package com.gondola.app.ws.request.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
	    "productOptions",
	    "productLists",
	    "productScenarios"
	})
public class Products {
	 private List<ProductOptions> productOptions;
	 private List<ProductLists> productLists;
	 private List<ProductScenarios> productScenarios;
	 
    @XmlElement(name = "ProductOptions")
	public List<ProductOptions> getProductOptions() {
		return productOptions;
	}
	public void setProductOptions(List<ProductOptions> productOptions) {
		this.productOptions = productOptions;
	}
	@XmlElement(name = "ProductLists")
	public List<ProductLists> getProductLists() {
		return productLists;
	}
	public void setProductLists(List<ProductLists> productLists) {
		this.productLists = productLists;
	}
	@XmlElement(name = "ProductScenarios")
	public List<ProductScenarios> getProductScenarios() {
		return productScenarios;
	}
	public void setProductScenarios(List<ProductScenarios> productScenarios) {
		this.productScenarios = productScenarios;
	}
	 
	 
}
