//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2022.08.04 at 09:56:52 AM UTC 
//


package com.gondola.app.ws.response.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompanyInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompanyInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NAIC" type="{urn:lifelink-schema}NAIC" minOccurs="0"/>
 *         &lt;element name="Address" type="{urn:lifelink-schema}Address" minOccurs="0"/>
 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Founded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Assets" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Liabilities" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ratings" type="{urn:lifelink-schema}Ratings" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompanyInfo", propOrder = {
    "name",
    "naic",
    "address",
    "phone",
    "url",
    "company",
    "founded",
    "dataDate",
    "rateDate",
    "assets",
    "liabilities",
    "ratings"
})
public class CompanyInfo {

    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "NAIC")
    protected NAIC naic;
    @XmlElement(name = "Address")
    protected Address address;
    @XmlElement(name = "Phone")
    protected String phone;
    @XmlElement(name = "URL")
    protected String url;
    @XmlElement(name = "Company")
    protected String company;
    @XmlElement(name = "Founded")
    protected String founded;
    @XmlElement(name = "DataDate")
    protected String dataDate;
    @XmlElement(name = "RateDate")
    protected String rateDate;
    @XmlElement(name = "Assets")
    protected String assets;
    @XmlElement(name = "Liabilities")
    protected String liabilities;
    @XmlElement(name = "Ratings")
    protected Ratings ratings;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the naic property.
     * 
     * @return
     *     possible object is
     *     {@link NAIC }
     *     
     */
    public NAIC getNAIC() {
        return naic;
    }

    /**
     * Sets the value of the naic property.
     * 
     * @param value
     *     allowed object is
     *     {@link NAIC }
     *     
     */
    public void setNAIC(NAIC value) {
        this.naic = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURL() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURL(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Gets the value of the founded property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFounded() {
        return founded;
    }

    /**
     * Sets the value of the founded property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFounded(String value) {
        this.founded = value;
    }

    /**
     * Gets the value of the dataDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataDate() {
        return dataDate;
    }

    /**
     * Sets the value of the dataDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataDate(String value) {
        this.dataDate = value;
    }

    /**
     * Gets the value of the rateDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateDate() {
        return rateDate;
    }

    /**
     * Sets the value of the rateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateDate(String value) {
        this.rateDate = value;
    }

    /**
     * Gets the value of the assets property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssets() {
        return assets;
    }

    /**
     * Sets the value of the assets property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssets(String value) {
        this.assets = value;
    }

    /**
     * Gets the value of the liabilities property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiabilities() {
        return liabilities;
    }

    /**
     * Sets the value of the liabilities property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiabilities(String value) {
        this.liabilities = value;
    }

    /**
     * Gets the value of the ratings property.
     * 
     * @return
     *     possible object is
     *     {@link Ratings }
     *     
     */
    public Ratings getRatings() {
        return ratings;
    }

    /**
     * Sets the value of the ratings property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ratings }
     *     
     */
    public void setRatings(Ratings value) {
        this.ratings = value;
    }

}
