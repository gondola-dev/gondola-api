//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2022.08.04 at 09:56:52 AM UTC 
//


package com.gondola.app.ws.response.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LL complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LL">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GroupPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WFCompanyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WFCompanyPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LOLAKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SessionID" type="{urn:lifelink-schema}SessionID" minOccurs="0"/>
 *         &lt;element name="Interface" type="{urn:lifelink-schema}InterfaceType"/>
 *         &lt;element name="Output" type="{urn:lifelink-schema}OutputType"/>
 *         &lt;element name="Buttons" type="{urn:lifelink-schema}Buttons" minOccurs="0"/>
 *         &lt;element name="ReturnButton" type="{urn:lifelink-schema}ReturnButton" minOccurs="0"/>
 *         &lt;element name="ReturnRedirURL" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Logout" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Tool" type="{urn:lifelink-schema}Tool" maxOccurs="unbounded"/>
 *         &lt;element name="InterfaceID" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/>
 *         &lt;element name="ReturnPDFString" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="LoginType" type="{urn:lifelink-schema}LoginType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LL", propOrder = {
    "groupName",
    "groupPassword",
    "userName",
    "userPassword",
    "wfCompanyCode",
    "wfCompanyPassword",
    "lolaKey",
    "sessionID",
    "_interface",
    "output",
    "buttons",
    "returnButton",
    "returnRedirURL",
    "logout",
    "tool",
    "interfaceID",
    "returnPDFString"
})
public class LL {

    @XmlElement(name = "GroupName")
    protected String groupName;
    @XmlElement(name = "GroupPassword")
    protected String groupPassword;
    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "UserPassword")
    protected String userPassword;
    @XmlElement(name = "WFCompanyCode")
    protected String wfCompanyCode;
    @XmlElement(name = "WFCompanyPassword")
    protected String wfCompanyPassword;
    @XmlElement(name = "LOLAKey")
    protected String lolaKey;
    @XmlElement(name = "SessionID")
    protected String sessionID;
    @XmlElement(name = "Interface", required = true)
    protected InterfaceType _interface;
    @XmlElement(name = "Output", required = true)
    protected OutputType output;
    @XmlElement(name = "Buttons")
    protected Buttons buttons;
    @XmlElement(name = "ReturnButton")
    protected ReturnButton returnButton;
    @XmlElement(name = "ReturnRedirURL")
    protected Boolean returnRedirURL;
    @XmlElement(name = "Logout")
    protected Boolean logout;
    @XmlElement(name = "Tool", required = true)
    protected List<Tool> tool;
    @XmlElement(name = "InterfaceID")
    protected Byte interfaceID;
    @XmlElement(name = "ReturnPDFString")
    protected Boolean returnPDFString;
    @XmlAttribute(name = "LoginType")
    protected LoginType loginType;

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the groupPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupPassword() {
        return groupPassword;
    }

    /**
     * Sets the value of the groupPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupPassword(String value) {
        this.groupPassword = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the userPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * Sets the value of the userPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPassword(String value) {
        this.userPassword = value;
    }

    /**
     * Gets the value of the wfCompanyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWFCompanyCode() {
        return wfCompanyCode;
    }

    /**
     * Sets the value of the wfCompanyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWFCompanyCode(String value) {
        this.wfCompanyCode = value;
    }

    /**
     * Gets the value of the wfCompanyPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWFCompanyPassword() {
        return wfCompanyPassword;
    }

    /**
     * Sets the value of the wfCompanyPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWFCompanyPassword(String value) {
        this.wfCompanyPassword = value;
    }

    /**
     * Gets the value of the lolaKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOLAKey() {
        return lolaKey;
    }

    /**
     * Sets the value of the lolaKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOLAKey(String value) {
        this.lolaKey = value;
    }

    /**
     * Gets the value of the sessionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionID() {
        return sessionID;
    }

    /**
     * Sets the value of the sessionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionID(String value) {
        this.sessionID = value;
    }

    /**
     * Gets the value of the interface property.
     * 
     * @return
     *     possible object is
     *     {@link InterfaceType }
     *     
     */
    public InterfaceType getInterface() {
        return _interface;
    }

    /**
     * Sets the value of the interface property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterfaceType }
     *     
     */
    public void setInterface(InterfaceType value) {
        this._interface = value;
    }

    /**
     * Gets the value of the output property.
     * 
     * @return
     *     possible object is
     *     {@link OutputType }
     *     
     */
    public OutputType getOutput() {
        return output;
    }

    /**
     * Sets the value of the output property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutputType }
     *     
     */
    public void setOutput(OutputType value) {
        this.output = value;
    }

    /**
     * Gets the value of the buttons property.
     * 
     * @return
     *     possible object is
     *     {@link Buttons }
     *     
     */
    public Buttons getButtons() {
        return buttons;
    }

    /**
     * Sets the value of the buttons property.
     * 
     * @param value
     *     allowed object is
     *     {@link Buttons }
     *     
     */
    public void setButtons(Buttons value) {
        this.buttons = value;
    }

    /**
     * Gets the value of the returnButton property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnButton }
     *     
     */
    public ReturnButton getReturnButton() {
        return returnButton;
    }

    /**
     * Sets the value of the returnButton property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnButton }
     *     
     */
    public void setReturnButton(ReturnButton value) {
        this.returnButton = value;
    }

    /**
     * Gets the value of the returnRedirURL property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnRedirURL() {
        return returnRedirURL;
    }

    /**
     * Sets the value of the returnRedirURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnRedirURL(Boolean value) {
        this.returnRedirURL = value;
    }

    /**
     * Gets the value of the logout property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLogout() {
        return logout;
    }

    /**
     * Sets the value of the logout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLogout(Boolean value) {
        this.logout = value;
    }

    /**
     * Gets the value of the tool property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tool property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTool().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tool }
     * 
     * 
     */
    public List<Tool> getTool() {
        if (tool == null) {
            tool = new ArrayList<Tool>();
        }
        return this.tool;
    }

    /**
     * Gets the value of the interfaceID property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getInterfaceID() {
        return interfaceID;
    }

    /**
     * Sets the value of the interfaceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setInterfaceID(Byte value) {
        this.interfaceID = value;
    }

    /**
     * Gets the value of the returnPDFString property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnPDFString() {
        return returnPDFString;
    }

    /**
     * Sets the value of the returnPDFString property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnPDFString(Boolean value) {
        this.returnPDFString = value;
    }

    /**
     * Gets the value of the loginType property.
     * 
     * @return
     *     possible object is
     *     {@link LoginType }
     *     
     */
    public LoginType getLoginType() {
        return loginType;
    }

    /**
     * Sets the value of the loginType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoginType }
     *     
     */
    public void setLoginType(LoginType value) {
        this.loginType = value;
    }

}
