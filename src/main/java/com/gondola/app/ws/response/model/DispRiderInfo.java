//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2022.08.04 at 09:56:52 AM UTC 
//


package com.gondola.app.ws.response.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DispRiderInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DispRiderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssueAges" type="{urn:lifelink-schema}MinMaxInfo" minOccurs="0"/>
 *         &lt;element name="FaceAmounts" type="{urn:lifelink-schema}MinMaxInfo" minOccurs="0"/>
 *         &lt;element name="MaturityAge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MaturityAgeInsured" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Units" type="{urn:lifelink-schema}MinMaxValueInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DispRiderInfo", propOrder = {
    "issueAges",
    "faceAmounts",
    "maturityAge",
    "maturityAgeInsured",
    "units"
})
public class DispRiderInfo {

    @XmlElement(name = "IssueAges")
    protected MinMaxInfo issueAges;
    @XmlElement(name = "FaceAmounts")
    protected MinMaxInfo faceAmounts;
    @XmlElement(name = "MaturityAge")
    protected Boolean maturityAge;
    @XmlElement(name = "MaturityAgeInsured")
    protected Boolean maturityAgeInsured;
    @XmlElement(name = "Units")
    protected MinMaxValueInfo units;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "id", required = true)
    protected long id;

    /**
     * Gets the value of the issueAges property.
     * 
     * @return
     *     possible object is
     *     {@link MinMaxInfo }
     *     
     */
    public MinMaxInfo getIssueAges() {
        return issueAges;
    }

    /**
     * Sets the value of the issueAges property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinMaxInfo }
     *     
     */
    public void setIssueAges(MinMaxInfo value) {
        this.issueAges = value;
    }

    /**
     * Gets the value of the faceAmounts property.
     * 
     * @return
     *     possible object is
     *     {@link MinMaxInfo }
     *     
     */
    public MinMaxInfo getFaceAmounts() {
        return faceAmounts;
    }

    /**
     * Sets the value of the faceAmounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinMaxInfo }
     *     
     */
    public void setFaceAmounts(MinMaxInfo value) {
        this.faceAmounts = value;
    }

    /**
     * Gets the value of the maturityAge property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaturityAge() {
        return maturityAge;
    }

    /**
     * Sets the value of the maturityAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaturityAge(Boolean value) {
        this.maturityAge = value;
    }

    /**
     * Gets the value of the maturityAgeInsured property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaturityAgeInsured() {
        return maturityAgeInsured;
    }

    /**
     * Sets the value of the maturityAgeInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaturityAgeInsured(Boolean value) {
        this.maturityAgeInsured = value;
    }

    /**
     * Gets the value of the units property.
     * 
     * @return
     *     possible object is
     *     {@link MinMaxValueInfo }
     *     
     */
    public MinMaxValueInfo getUnits() {
        return units;
    }

    /**
     * Sets the value of the units property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinMaxValueInfo }
     *     
     */
    public void setUnits(MinMaxValueInfo value) {
        this.units = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

}
