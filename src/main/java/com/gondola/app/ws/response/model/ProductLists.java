//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2022.08.04 at 09:56:52 AM UTC 
//


package com.gondola.app.ws.response.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductLists complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductLists">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="YearsLevel" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProductsRequested" type="{urn:lifelink-schema}ProductRequest" minOccurs="0"/>
 *         &lt;element name="Product" type="{urn:lifelink-schema}ProductType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GuaranteedPeriod" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductLists", propOrder = {
    "listName",
    "yearsLevel",
    "productsRequested",
    "product",
    "guaranteedPeriod"
})
public class ProductLists {

    @XmlElement(name = "ListName")
    protected String listName;
    @XmlElement(name = "YearsLevel", type = Integer.class)
    protected List<Integer> yearsLevel;
    @XmlElement(name = "ProductsRequested")
    protected ProductRequest productsRequested;
    @XmlElement(name = "Product")
    protected List<ProductType> product;
    @XmlElement(name = "GuaranteedPeriod", type = Integer.class)
    protected List<Integer> guaranteedPeriod;

    /**
     * Gets the value of the listName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListName() {
        return listName;
    }

    /**
     * Sets the value of the listName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListName(String value) {
        this.listName = value;
    }

    /**
     * Gets the value of the yearsLevel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the yearsLevel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getYearsLevel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getYearsLevel() {
        if (yearsLevel == null) {
            yearsLevel = new ArrayList<Integer>();
        }
        return this.yearsLevel;
    }

    /**
     * Gets the value of the productsRequested property.
     * 
     * @return
     *     possible object is
     *     {@link ProductRequest }
     *     
     */
    public ProductRequest getProductsRequested() {
        return productsRequested;
    }

    /**
     * Sets the value of the productsRequested property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductRequest }
     *     
     */
    public void setProductsRequested(ProductRequest value) {
        this.productsRequested = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the product property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProduct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductType }
     * 
     * 
     */
    public List<ProductType> getProduct() {
        if (product == null) {
            product = new ArrayList<ProductType>();
        }
        return this.product;
    }

    /**
     * Gets the value of the guaranteedPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the guaranteedPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGuaranteedPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getGuaranteedPeriod() {
        if (guaranteedPeriod == null) {
            guaranteedPeriod = new ArrayList<Integer>();
        }
        return this.guaranteedPeriod;
    }

}
