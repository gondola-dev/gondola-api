//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2022.08.04 at 09:56:52 AM UTC 
//


package com.gondola.app.ws.response.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Button complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Button">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Type" type="{urn:lifelink-schema}Button"/>
 *         &lt;element name="ViewFirst" type="{urn:lifelink-schema}ViewFirst" minOccurs="0"/>
 *         &lt;element name="PostAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostParameterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ButtonAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="clicked" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Button", propOrder = {
    "text",
    "type",
    "viewFirst",
    "postAddress",
    "postParameterName",
    "buttonAddress"
})
public class Button {

    @XmlElement(name = "Text", required = true)
    protected String text;
    @XmlElement(name = "Type", required = true)
    protected Button type;
    @XmlElement(name = "ViewFirst")
    protected Integer viewFirst;
    @XmlElement(name = "PostAddress")
    protected String postAddress;
    @XmlElement(name = "PostParameterName")
    protected String postParameterName;
    @XmlElement(name = "ButtonAddress")
    protected String buttonAddress;
    @XmlAttribute(name = "clicked")
    protected Boolean clicked;

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link Button }
     *     
     */
    public Button getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link Button }
     *     
     */
    public void setType(Button value) {
        this.type = value;
    }

    /**
     * Gets the value of the viewFirst property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getViewFirst() {
        return viewFirst;
    }

    /**
     * Sets the value of the viewFirst property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setViewFirst(Integer value) {
        this.viewFirst = value;
    }

    /**
     * Gets the value of the postAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostAddress() {
        return postAddress;
    }

    /**
     * Sets the value of the postAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostAddress(String value) {
        this.postAddress = value;
    }

    /**
     * Gets the value of the postParameterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostParameterName() {
        return postParameterName;
    }

    /**
     * Sets the value of the postParameterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostParameterName(String value) {
        this.postParameterName = value;
    }

    /**
     * Gets the value of the buttonAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getButtonAddress() {
        return buttonAddress;
    }

    /**
     * Sets the value of the buttonAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setButtonAddress(String value) {
        this.buttonAddress = value;
    }

    /**
     * Gets the value of the clicked property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isClicked() {
        return clicked;
    }

    /**
     * Sets the value of the clicked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClicked(Boolean value) {
        this.clicked = value;
    }

}
