//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2022.08.04 at 09:56:52 AM UTC 
//


package com.gondola.app.ws.response.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DetermiatorAnswerXML complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DetermiatorAnswerXML">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Question" type="{urn:lifelink-schema}DetQuestion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RunXRaeOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RunVSSOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RunFILIOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetermiatorAnswerXML", propOrder = {
    "question",
    "runXRaeOnly",
    "runVSSOnly",
    "runFILIOnly"
})
public class DetermiatorAnswerXML {

    @XmlElement(name = "Question")
    protected List<DetQuestion> question;
    @XmlElement(name = "RunXRaeOnly")
    protected Boolean runXRaeOnly;
    @XmlElement(name = "RunVSSOnly")
    protected Boolean runVSSOnly;
    @XmlElement(name = "RunFILIOnly")
    protected Boolean runFILIOnly;

    /**
     * Gets the value of the question property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the question property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuestion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DetQuestion }
     * 
     * 
     */
    public List<DetQuestion> getQuestion() {
        if (question == null) {
            question = new ArrayList<DetQuestion>();
        }
        return this.question;
    }

    /**
     * Gets the value of the runXRaeOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRunXRaeOnly() {
        return runXRaeOnly;
    }

    /**
     * Sets the value of the runXRaeOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRunXRaeOnly(Boolean value) {
        this.runXRaeOnly = value;
    }

    /**
     * Gets the value of the runVSSOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRunVSSOnly() {
        return runVSSOnly;
    }

    /**
     * Sets the value of the runVSSOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRunVSSOnly(Boolean value) {
        this.runVSSOnly = value;
    }

    /**
     * Gets the value of the runFILIOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRunFILIOnly() {
        return runFILIOnly;
    }

    /**
     * Sets the value of the runFILIOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRunFILIOnly(Boolean value) {
        this.runFILIOnly = value;
    }

}
