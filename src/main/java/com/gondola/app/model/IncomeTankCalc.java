package com.gondola.app.model;

import java.io.Serializable;

public class IncomeTankCalc implements Serializable {
	
	private static final long serialVersionUID = -2405157489714531955L;
	private long potentialamount;
	private long maxprotectamount;
	private long quoteamount;

	public long getPotentialamount() {
		return potentialamount;
	}

	public void setPotentialamount(long potentialamount) {
		this.potentialamount = potentialamount;
	}

	public long getMaxprotectamount() {
		return maxprotectamount;
	}

	public void setMaxprotectamount(long maxprotectamount) {
		this.maxprotectamount = maxprotectamount;
	}

	public long getQuoteamount() {
		return quoteamount;
	}

	public void setQuoteamount(long quoteamount) {
		this.quoteamount = quoteamount;
	}


}
