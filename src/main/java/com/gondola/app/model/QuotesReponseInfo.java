package com.gondola.app.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class QuotesReponseInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4637551723451715732L;
	private int numberOfProductsFiltered = 0;
	private int numberOfProductsQuoted = 0;
	private Set<ProductListDetails> totalProductList;
	private Set<ProductListDetails> filteredProductList;
	private List<ProductListDetails> topThreeProductList;
	private IncomeTankCalc incomeTankCalc;
	private String status;
	private String pageName;

	public int getNumberOfProductsFiltered() {
		return numberOfProductsFiltered;
	}

	public void setNumberOfProductsFiltered(int numberOfProductsFiltered) {
		this.numberOfProductsFiltered = numberOfProductsFiltered;
	}

	public int getNumberOfProductsQuoted() {
		return numberOfProductsQuoted;
	}

	public void setNumberOfProductsQuoted(int numberOfProductsQuoted) {
		this.numberOfProductsQuoted = numberOfProductsQuoted;
	}

	public Set<ProductListDetails> getTotalProductList() {
		return totalProductList;
	}

	public void setTotalProductList(Set<ProductListDetails> totalProductList) {
		this.totalProductList = totalProductList;
	}

	public Set<ProductListDetails> getFilteredProductList() {
		return filteredProductList;
	}

	public void setFilteredProductList(Set<ProductListDetails> filteredProductList) {
		this.filteredProductList = filteredProductList;
	}

	public List<ProductListDetails> getTopThreeProductList() {
		return topThreeProductList;
	}

	public void setTopThreeProductList(List<ProductListDetails> topThreeProductList) {
		this.topThreeProductList = topThreeProductList;
	}

	public IncomeTankCalc getIncomeTankCalc() {
		return incomeTankCalc;
	}

	public void setIncomeTankCalc(IncomeTankCalc incomeTankCalc) {
		this.incomeTankCalc = incomeTankCalc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

}
