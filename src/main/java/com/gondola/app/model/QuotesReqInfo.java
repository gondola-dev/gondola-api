package com.gondola.app.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuotesReqInfo implements Serializable {
	private static final long serialVersionUID = 3908184142611549221L;
	private long subscriberid;
	private String emailaddress;
	private long amount;
	private int years;
	private long annualincome;
	private int expectedretirementage;
	private long estAssets;
	private String issmoker;
	private String isWorking;
	private int age;
	private String statecode;
	private String gender;
	private boolean advancefilter;
	private List<String> addonslist;
	private String withNaic;
	private boolean maxprotectcall;
	private String ipaddress;
	private boolean prod;

	public long getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(long subscriberid) {
		this.subscriberid = subscriberid;
	}

	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public long getAnnualincome() {
		return annualincome;
	}

	public void setAnnualincome(long annualincome) {
		this.annualincome = annualincome;
	}

	public int getExpectedretirementage() {
		return expectedretirementage;
	}

	public void setExpectedretirementage(int expectedretirementage) {
		this.expectedretirementage = expectedretirementage;
	}

	public long getEstAssets() {
		return estAssets;
	}

	public void setEstAssets(long estAssets) {
		this.estAssets = estAssets;
	}

	public String getIssmoker() {
		return issmoker;
	}

	public void setIssmoker(String issmoker) {
		this.issmoker = issmoker;
	}

	public String getIsWorking() {
		return isWorking;
	}

	public void setIsWorking(String isWorking) {
		this.isWorking = isWorking;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getStatecode() {
		return statecode;
	}

	public void setStatecode(String statecode) {
		this.statecode = statecode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isAdvancefilter() {
		return advancefilter;
	}

	public void setAdvancefilter(boolean advancefilter) {
		this.advancefilter = advancefilter;
	}

	public List<String> getAddonslist() {
		return addonslist;
	}

	public void setAddonslist(List<String> addonslist) {
		this.addonslist = addonslist;
	}

	public String getWithNaic() {
		return withNaic;
	}

	public void setWithNaic(String withNaic) {
		this.withNaic = withNaic;
	}

	public boolean isMaxprotectcall() {
		return maxprotectcall;
	}

	public void setMaxprotectcall(boolean maxprotectcall) {
		this.maxprotectcall = maxprotectcall;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public boolean isProd() {
		return prod;
	}

	public void setProd(boolean prod) {
		this.prod = prod;
	}

}
