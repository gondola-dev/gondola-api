package com.gondola.app.model;

import java.io.Serializable;
import java.util.List;

import com.gondola.app.ws.response.model.Address;
import com.gondola.app.ws.response.model.Ratings;
import com.gondola.app.ws.response.model.YearValue;

public class ProductListDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 900297808511740856L; 
	private long productid;
	private String productname;
	private boolean isProductNAICCertified;
	private String productYearsLevel;
	private String productGuarenteedYears;
	private int productMaxYearsToShow;
	private String productMinAgeIssued;
	private String productMaxAgeIssued;
	private String productMinFaceAmount;
	private String productMaxFaceAmount;
	private String companyName;
	private Address companyAddress;
	private String companyphonenumber;
	private String companyfounded;
	private String companywebsite;
	private String companyNAICCode;
	private String isCompanyNAICCertified;
	private String guaranteedMonthlyPremiumAmount;
	private String guaranteedQuarterlyPremiumAmount;
	private String guaranteedSemiAnnualPremiumAmount;
	private String guaranteedAnnualPremiumAmount;
	private List<YearValue> totalpremiumvalues;
	private String companyRating;
	private int companyRatingId;
	private Ratings allcompanyratings;
	private String faceAmount;
	private String underWritingClass;
	private Integer underWritingClassId;
	private Integer tableRatingId;
	private Integer healthRatingId;
	private String healthRatingName;
	private int speedToProtectionid;
	private String speedToProtectionname;
	private int waiveraddonsid;
	private String waiveraddonsname;
	private int adbaddonsid;
	private String adbaddonsname;
	private int childaddonsid;
	private String childaddonsname;
	private String conversionname;
	private boolean isconversion;
	private boolean isTopPick;
	private long potentialamount;

	public long getProductid() {
		return productid;
	}

	public void setProductid(long productid) {
		this.productid = productid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public boolean isProductNAICCertified() {
		return isProductNAICCertified;
	}

	public void setProductNAICCertified(boolean isProductNAICCertified) {
		this.isProductNAICCertified = isProductNAICCertified;
	}

	public String getProductYearsLevel() {
		return productYearsLevel;
	}

	public void setProductYearsLevel(String productYearsLevel) {
		this.productYearsLevel = productYearsLevel;
	}

	public String getProductGuarenteedYears() {
		return productGuarenteedYears;
	}

	public void setProductGuarenteedYears(String productGuarenteedYears) {
		this.productGuarenteedYears = productGuarenteedYears;
	}

	public int getProductMaxYearsToShow() {
		return productMaxYearsToShow;
	}

	public void setProductMaxYearsToShow(int productMaxYearsToShow) {
		this.productMaxYearsToShow = productMaxYearsToShow;
	}

	public String getProductMinAgeIssued() {
		return productMinAgeIssued;
	}

	public void setProductMinAgeIssued(String productMinAgeIssued) {
		this.productMinAgeIssued = productMinAgeIssued;
	}

	public String getProductMaxAgeIssued() {
		return productMaxAgeIssued;
	}

	public void setProductMaxAgeIssued(String productMaxAgeIssued) {
		this.productMaxAgeIssued = productMaxAgeIssued;
	}

	public String getProductMinFaceAmount() {
		return productMinFaceAmount;
	}

	public void setProductMinFaceAmount(String productMinFaceAmount) {
		this.productMinFaceAmount = productMinFaceAmount;
	}

	public String getProductMaxFaceAmount() {
		return productMaxFaceAmount;
	}

	public void setProductMaxFaceAmount(String productMaxFaceAmount) {
		this.productMaxFaceAmount = productMaxFaceAmount;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Address getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Address companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyphonenumber() {
		return companyphonenumber;
	}

	public void setCompanyphonenumber(String companyphonenumber) {
		this.companyphonenumber = companyphonenumber;
	}

	public String getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(String faceAmount) {
		this.faceAmount = faceAmount;
	}

	public String getCompanyfounded() {
		return companyfounded;
	}

	public void setCompanyfounded(String companyfounded) {
		this.companyfounded = companyfounded;
	}

	public String getCompanywebsite() {
		return companywebsite;
	}

	public void setCompanywebsite(String companywebsite) {
		this.companywebsite = companywebsite;
	}

	public String getCompanyNAICCode() {
		return companyNAICCode;
	}

	public void setCompanyNAICCode(String companyNAICCode) {
		this.companyNAICCode = companyNAICCode;
	}

	public String getIsCompanyNAICCertified() {
		return isCompanyNAICCertified;
	}

	public void setIsCompanyNAICCertified(String isCompanyNAICCertified) {
		this.isCompanyNAICCertified = isCompanyNAICCertified;
	}

	public String getGuaranteedMonthlyPremiumAmount() {
		return guaranteedMonthlyPremiumAmount;
	}

	public void setGuaranteedMonthlyPremiumAmount(String guaranteedMonthlyPremiumAmount) {
		this.guaranteedMonthlyPremiumAmount = guaranteedMonthlyPremiumAmount;
	}

	public String getGuaranteedQuarterlyPremiumAmount() {
		return guaranteedQuarterlyPremiumAmount;
	}

	public void setGuaranteedQuarterlyPremiumAmount(String guaranteedQuarterlyPremiumAmount) {
		this.guaranteedQuarterlyPremiumAmount = guaranteedQuarterlyPremiumAmount;
	}

	public String getGuaranteedSemiAnnualPremiumAmount() {
		return guaranteedSemiAnnualPremiumAmount;
	}

	public void setGuaranteedSemiAnnualPremiumAmount(String guaranteedSemiAnnualPremiumAmount) {
		this.guaranteedSemiAnnualPremiumAmount = guaranteedSemiAnnualPremiumAmount;
	}

	public String getGuaranteedAnnualPremiumAmount() {
		return guaranteedAnnualPremiumAmount;
	}

	public void setGuaranteedAnnualPremiumAmount(String guaranteedAnnualPremiumAmount) {
		this.guaranteedAnnualPremiumAmount = guaranteedAnnualPremiumAmount;
	}

	public List<YearValue> getTotalpremiumvalues() {
		return totalpremiumvalues;
	}

	public void setTotalpremiumvalues(List<YearValue> totalpremiumvalues) {
		this.totalpremiumvalues = totalpremiumvalues;
	}

	public String getCompanyRating() {
		return companyRating;
	}

	public void setCompanyRating(String companyRating) {
		this.companyRating = companyRating;
	}

	public int getCompanyRatingId() {
		return companyRatingId;
	}

	public void setCompanyRatingId(int companyRatingId) {
		this.companyRatingId = companyRatingId;
	}

	public Ratings getAllcompanyratings() {
		return allcompanyratings;
	}

	public void setAllcompanyratings(Ratings allcompanyratings) {
		this.allcompanyratings = allcompanyratings;
	}

	public Integer getTableRatingId() {
		return tableRatingId;
	}

	public void setTableRatingId(Integer tableRatingId) {
		this.tableRatingId = tableRatingId;
	}

	public String getUnderWritingClass() {
		return underWritingClass;
	}

	public void setUnderWritingClass(String underWritingClass) {
		this.underWritingClass = underWritingClass;
	}

	public Integer getUnderWritingClassId() {
		return underWritingClassId;
	}

	public void setUnderWritingClassId(Integer underWritingClassId) {
		this.underWritingClassId = underWritingClassId;
	}

	public Integer getHealthRatingId() {
		return healthRatingId;
	}

	public void setHealthRatingId(Integer healthRatingId) {
		this.healthRatingId = healthRatingId;
	}

	public String getHealthRatingName() {
		return healthRatingName;
	}

	public void setHealthRatingName(String healthRatingName) {
		this.healthRatingName = healthRatingName;
	}

	public int getSpeedToProtectionid() {
		return speedToProtectionid;
	}

	public void setSpeedToProtectionid(int speedToProtectionid) {
		this.speedToProtectionid = speedToProtectionid;
	}

	public String getSpeedToProtectionname() {
		return speedToProtectionname;
	}

	public void setSpeedToProtectionname(String speedToProtectionname) {
		this.speedToProtectionname = speedToProtectionname;
	}

	public int getWaiveraddonsid() {
		return waiveraddonsid;
	}

	public void setWaiveraddonsid(int waiveraddonsid) {
		this.waiveraddonsid = waiveraddonsid;
	}

	public String getWaiveraddonsname() {
		return waiveraddonsname;
	}

	public void setWaiveraddonsname(String waiveraddonsname) {
		this.waiveraddonsname = waiveraddonsname;
	}

	public int getAdbaddonsid() {
		return adbaddonsid;
	}

	public void setAdbaddonsid(int adbaddonsid) {
		this.adbaddonsid = adbaddonsid;
	}

	public String getAdbaddonsname() {
		return adbaddonsname;
	}

	public void setAdbaddonsname(String adbaddonsname) {
		this.adbaddonsname = adbaddonsname;
	}

	public int getChildaddonsid() {
		return childaddonsid;
	}

	public void setChildaddonsid(int childaddonsid) {
		this.childaddonsid = childaddonsid;
	}

	public String getChildaddonsname() {
		return childaddonsname;
	}

	public void setChildaddonsname(String childaddonsname) {
		this.childaddonsname = childaddonsname;
	}

	public String getConversionname() {
		return conversionname;
	}

	public void setConversionname(String conversionname) {
		this.conversionname = conversionname;
	}

	public boolean isIsconversion() {
		return isconversion;
	}

	public void setIsconversion(boolean isconversion) {
		this.isconversion = isconversion;
	}

	public boolean isTopPick() {
		return isTopPick;
	}

	public void setTopPick(boolean isTopPick) {
		this.isTopPick = isTopPick;
	}

	public long getPotentialamount() {
		return potentialamount;
	}

	public void setPotentialamount(long potentialamount) {
		this.potentialamount = potentialamount;
	}

	

}
