package com.gondola.app.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubscriberResInfo implements Serializable {
	
	private static final long serialVersionUID = -4112402213722737093L;
	private String status;
	private String pageName;
	private String firstName;
	private String emailAddress;
	private String mobileNumber;
	private int subscriberid;
	private String ipaddress;
	private boolean isDataExist;
	private List<String> errorList;
	private List<QuotesReqInfo> quoteList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(int subscriberid) {
		this.subscriberid = subscriberid;
	}

	public boolean isDataExist() {
		return isDataExist;
	}

	public void setDataExist(boolean isDataExist) {
		this.isDataExist = isDataExist;
	}

	public List<String> getErrorList() {
		return errorList;
	}

	public List<QuotesReqInfo> getQuoteList() {
		return quoteList;
	}

	public void setQuoteList(List<QuotesReqInfo> quoteList) {
		this.quoteList = quoteList;
	}

	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

}
