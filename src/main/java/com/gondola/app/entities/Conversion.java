package com.gondola.app.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "conversion", schema = "gondola")
public class Conversion {
	private int id;
	private String careername;
	private String productname;
	private int productid;
	private String conversionname;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() { 
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCareername() {
		return careername;
	}

	public void setCareername(String careername) {
		this.careername = careername;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public String getConversionname() {
		return conversionname;
	}

	public void setConversionname(String conversionname) {
		this.conversionname = conversionname;
	}

	@Override
	public String toString() {
		return "Conversion [id=" + id + ", careername=" + careername + ", productname=" + productname
				+ ", productid=" + productid + ", conversionname=" + conversionname + "]";
	}
}
