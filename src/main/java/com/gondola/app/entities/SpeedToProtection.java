package com.gondola.app.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "speedtoprotection", schema = "gondola")
public class SpeedToProtection {
	private int id;
	private int naiccode;
	private int speedtoprotectid;
	private String speedtoprotectionname;

	@Id
	public int getId() { 
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNaiccode() {
		return naiccode;
	}

	public void setNaiccode(int naiccode) {
		this.naiccode = naiccode;
	}

	public int getSpeedtoprotectid() {
		return speedtoprotectid;
	}

	public void setSpeedtoprotectid(int speedtoprotectid) {
		this.speedtoprotectid = speedtoprotectid;
	}

	public String getSpeedtoprotectionname() {
		return speedtoprotectionname;
	}

	public void setSpeedtoprotectionname(String speedtoprotectionname) {
		this.speedtoprotectionname = speedtoprotectionname;
	}

	@Override
	public String toString() {
		return "SpeedToProtection [id=" + id + ", naiccode=" + naiccode + ", speedtoprotectid=" + speedtoprotectid
				+ ", speedtoprotectionname=" + speedtoprotectionname + "]";
	}

}
