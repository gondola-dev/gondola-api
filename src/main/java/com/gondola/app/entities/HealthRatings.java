package com.gondola.app.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "healthratings", schema = "gondola")
public class HealthRatings {
	private int id;
	private int ratingid;
	private String ratingname;
	private String gondolaclass;
	private String underwritingclass;
	private int status;

	@Id
	public int getId() { 
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRatingid() {
		return ratingid;
	}

	public void setRatingid(int ratingid) {
		this.ratingid = ratingid;
	}

	public String getRatingname() {
		return ratingname;
	}

	public void setRatingname(String ratingname) {
		this.ratingname = ratingname;
	}

	public String getGondolaclass() {
		return gondolaclass;
	}

	public void setGondolaclass(String gondolaclass) {
		this.gondolaclass = gondolaclass;
	}

	public String getUnderwritingclass() {
		return underwritingclass;
	}

	public void setUnderwritingclass(String underwritingclass) {
		this.underwritingclass = underwritingclass;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "HealthRatings [ratingid=" + ratingid + ", ratingname=" + ratingname + ", gondolaclass=" + gondolaclass
				+ ", underwritingclass=" + underwritingclass + "]";
	}

}
