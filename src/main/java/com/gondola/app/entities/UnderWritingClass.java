package com.gondola.app.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "underwritingclass", schema = "gondola")
public class UnderWritingClass {
	private long id;
	private String underwritingclassname;
	private int underwritingclassid;

	public UnderWritingClass() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUnderwritingclassname() {
		return underwritingclassname;
	}

	public void setUnderwritingclassname(String underwritingclassname) {
		this.underwritingclassname = underwritingclassname;
	}

	public int getUnderwritingclassid() {
		return underwritingclassid;
	}

	public void setUnderwritingclassid(int underwritingclassid) {
		this.underwritingclassid = underwritingclassid;
	}

}