package com.gondola.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "naiclist" , schema = "gondola")
public class NaicList {
   
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "naiccode")
	private String naicCode;
	@Column(name = "carriername")
	private String carrierName;
	@Column(name = "status")
	private int status;
	
	public String getNaicCode() {
		return naicCode;
	}
	public void setNaicCode(String naicCode) {
		this.naicCode = naicCode;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "NaicList [naicCode=" + naicCode + ", carrierName=" + carrierName + ", status=" + status + "]";
	}
}
