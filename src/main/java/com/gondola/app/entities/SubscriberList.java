package com.gondola.app.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "createsubscribers", schema = "gondola")
public class SubscriberList {
	private int id;
	private String emailaddress;
	private String ipaddress;
	private String firstname;
	private String mobilenumber;
	private String otpprefix;
	private String otpnumber;
	private String status;
	private Timestamp createddate;
	private Timestamp modifieddate;

	public SubscriberList() {

	}

	public SubscriberList(String emailaddress, String otpprefix, String otpnumber, String status,
			Timestamp modifieddate) {
		super();
		this.otpprefix = otpprefix;
		this.otpnumber = otpnumber;
		this.status = status;
		this.modifieddate = modifieddate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "subscriberid")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMobilenumber() {
		return mobilenumber;
	}

	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}

	public String getOtpprefix() {
		return otpprefix;
	}

	public void setOtpprefix(String otpprefix) {
		this.otpprefix = otpprefix;
	}

	public String getOtpnumber() {
		return otpnumber;
	}

	public void setOtpnumber(String otpnumber) {
		this.otpnumber = otpnumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}

	public Timestamp getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}

	@Override
	public String toString() {
		return "SubscriberList [id=" + id + ", emailaddress=" + emailaddress + ", ipaddress=" + ipaddress
				+ ", firstname=" + firstname + ", mobilenumber=" + mobilenumber + ", otpprefix=" + otpprefix
				+ ", otpnumber=" + otpnumber + ", status=" + status + ", createddate=" + createddate + ", modifieddate="
				+ modifieddate + "]";
	}

}
