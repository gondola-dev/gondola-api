package com.gondola.app.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "companyratings", schema = "gondola")
public class CompanyRatings {
	private int id;
	private String ratingname;

	@Id
	public int getId() { 
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRatingname() {
		return ratingname;
	}

	public void setRatingname(String ratingname) {
		this.ratingname = ratingname;
	}

	@Override
	public String toString() {
		return "CompanyRatings [id=" + id + ", ratingname=" + ratingname + "]";
	}


}
