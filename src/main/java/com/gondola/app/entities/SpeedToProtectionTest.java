package com.gondola.app.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "speedtoprotectiontest", schema = "gondola")
public class SpeedToProtectionTest {
	private int id;
	private String careername;
	private String careershortname;
	private int naiccode;
	private int speedtoprotectid;
	private String speedtoprotectionname;
	private int uwprogram;
	private int uwspeed;

	@Id
	public int getId() { 
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCareername() {
		return careername;
	}

	public void setCareername(String careername) {
		this.careername = careername;
	}

	public String getCareershortname() {
		return careershortname;
	}

	public void setCareershortname(String careershortname) {
		this.careershortname = careershortname;
	}

	public int getNaiccode() {
		return naiccode;
	}

	public void setNaiccode(int naiccode) {
		this.naiccode = naiccode;
	}

	public int getSpeedtoprotectid() {
		return speedtoprotectid;
	}

	public void setSpeedtoprotectid(int speedtoprotectid) {
		this.speedtoprotectid = speedtoprotectid;
	}

	public String getSpeedtoprotectionname() {
		return speedtoprotectionname;
	}

	public void setSpeedtoprotectionname(String speedtoprotectionname) {
		this.speedtoprotectionname = speedtoprotectionname;
	}

	public int getUwprogram() {
		return uwprogram;
	}

	public void setUwprogram(int uwprogram) {
		this.uwprogram = uwprogram;
	}

	public int getUwspeed() {
		return uwspeed;
	}

	public void setUwspeed(int uwspeed) {
		this.uwspeed = uwspeed;
	}

	@Override
	public String toString() {
		return "SpeedToProtectionTest [id=" + id + ", careername=" + careername + ", careershortname=" + careershortname
				+ ", naiccode=" + naiccode + ", speedtoprotectid=" + speedtoprotectid + ", speedtoprotectionname="
				+ speedtoprotectionname + ", uwprogram=" + uwprogram + ", uwspeed=" + uwspeed + "]";
	}

}
