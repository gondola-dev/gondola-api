package com.gondola.app.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "createquotes", schema = "gondola")
public class QuotesInfo {
	private long id;
	private long subscriberid;
	private String emailaddress;
	private long amount;
	private int years;
	private long annualincome;
	private int expectedretirementage;
	private long estAssets;
	private String issmoker;
	private String isWorking;
	private int age;
	private String statecode;
	private String gender;
	private String ipaddress;
	private boolean advancefilter;
	private String addonslist;
	private Timestamp createdinfo;

	public QuotesInfo() {
	}

	public QuotesInfo(long id, long subscriberid, String emailaddress, long amount, int years, long annualincome,
			int expectedretirementage, long estAssets, String issmoker, String isWorking, int age, String statename,
			String gender, String ipaddress, boolean advancefilter, String addonslist) {
		super();
		this.id = id;
		this.subscriberid = subscriberid;
		this.emailaddress = emailaddress;
		this.amount = amount;
		this.years = years;
		this.annualincome = annualincome;
		this.expectedretirementage = expectedretirementage;
		this.estAssets = estAssets;
		this.issmoker = issmoker;
		this.isWorking = isWorking;
		this.age = age;
		this.statecode = statename;
		this.gender = gender;
		this.ipaddress = ipaddress;
		this.advancefilter = advancefilter;
		this.addonslist = addonslist;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "quote_key_pk")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "subscriberid_bigint")
	public long getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(long subscriberid) {
		this.subscriberid = subscriberid;
	}

	@Column(name = "emailaddress_varchar")
	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	@Column(name = "annual_income_bigint")
	public long getAnnualincome() {
		return annualincome;
	}

	public void setAnnualincome(long annualincome) {
		this.annualincome = annualincome;
	}

	@Column(name = "expected_retirement_age_smint")
	public int getExpectedretirementage() {
		return expectedretirementage;
	}

	public void setExpectedretirementage(int expectedretirementage) {
		this.expectedretirementage = expectedretirementage;
	}

	@Column(name = "age_smint")
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setStatecode(String statecode) {
		this.statecode = statecode;
	}
	
	@Column(name = "state_name_varchar")
	public String getStatecode() {
		return statecode;
	}

	@Column(name = "gender_varchar")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "amount_bigint")
	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	@Column(name = "years_smint")
	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	@Column(name = "issmoker_varchar")
	public String getIssmoker() {
		return issmoker;
	}

	public void setIssmoker(String issmoker) {
		this.issmoker = issmoker;
	}

	@Column(name = "isworking_varchar")
	public String getIsWorking() {
		return isWorking;
	}

	public void setIsWorking(String isWorking) {
		this.isWorking = isWorking;
	}

	@Column(name = "ipaddress_varchar")
	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	@Column(name = "advancefilter_boolean")
	public boolean isAdvancefilter() {
		return advancefilter;
	}

	public void setAdvancefilter(boolean advancefilter) {
		this.advancefilter = advancefilter;
	}

	@Column(name = "addonslist_varchar")
	public String getAddonslist() {
		return addonslist;
	}

	public void setAddonslist(String addonslist) {
		this.addonslist = addonslist;
	}

	@Column(name = "estassets_bigint")
	public long getEstAssets() {
		return estAssets;
	}

	public void setEstAssets(long estAssets) {
		this.estAssets = estAssets;
	}

	@Column(name = "created_ts")
	@CreationTimestamp
	public Timestamp getCreatedinfo() {
		return createdinfo;
	}

	public void setCreatedinfo(Timestamp createdinfo) {
		this.createdinfo = createdinfo;
	}

	@Override
	public String toString() {
		return "QuotesInfo [id=" + id + ", subscriberid=" + subscriberid + ", emailaddress=" + emailaddress
				+ ", amount=" + amount + ", years=" + years + ", annualincome=" + annualincome
				+ ", expectedretirementage=" + expectedretirementage + ", estAssets=" + estAssets + ", issmoker="
				+ issmoker + ", isWorking=" + isWorking + ", age=" + age + ", statecode=" + statecode + ", gender="
				+ gender + ", ipaddress=" + ipaddress + ", advancefilter=" + advancefilter + ", addonslist="
				+ addonslist + ", createdinfo=" + createdinfo + "]";
	}

	/*
	 * Create TABLE gondola.quoteinfo( quote_key_pk SERIAL PRIMARY KEY NOT NULL,
	 * gender_varchar character varying(10), age_smint smallint, state_name_varchar
	 * character varying(50), issmoker_varchar character varying(10),
	 * expected_retirement_age_smint smallint, annual_income_deci numeric,
	 * amount_deci numeric, years_smint smallint, ipaddress_varchar character
	 * varying(32), created_ts timestamp with time zone default current_timestamp );
	 */
}