package com.gondola.app.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "insurerlist", schema = "gondola")
public class InsurerList {
	private int id;
	private String insurername;
	private String insurershortname;
	private int naiccode;

	@Id
	public int getId() { 
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInsurername() {
		return insurername;
	}

	public void setInsurername(String insurername) {
		this.insurername = insurername;
	}

	public String getInsurershortname() {
		return insurershortname;
	}

	public void setInsurershortname(String insurershortname) {
		this.insurershortname = insurershortname;
	}

	public int getNaiccode() {
		return naiccode;
	}

	public void setNaiccode(int naiccode) {
		this.naiccode = naiccode;
	}

	@Override
	public String toString() {
		return "InsurerList [id=" + id + ", insurername=" + insurername + ", insurershortname=" + insurershortname
				+ ", naiccode=" + naiccode + "]";
	}


}
