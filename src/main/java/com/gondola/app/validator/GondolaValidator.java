package com.gondola.app.validator;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.gondola.app.model.SubscriberReqInfo;

@Component
public class GondolaValidator {
	
	private static Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");

	public void validate(List<String> errorList, SubscriberReqInfo reqInfo) {
		if(null != reqInfo.getEmail_address()){
			emailValidate(errorList,reqInfo.getEmail_address());
			int subScriberId = reqInfo.getEmail_address().hashCode();
			reqInfo.setSubscriberid(subScriberId);
		}
	}
	
	private void emailValidate(List<String> errorList, String email) {
		if(!isValidEmail(email)){
			errorList.add("invalidEmail");
		}
	}
	
	public static boolean isValidEmail(String emailID) {
		if(!emailID.isEmpty()){
			Matcher matcher = emailPattern.matcher(emailID.trim());
			if(!matcher.matches()) {
				return false;
			}
		}
		return true;
	}
}
