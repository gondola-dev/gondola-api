package com.gondola.app.transformer;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import com.gondola.app.entities.SubscriberList;
import com.gondola.app.model.SubscriberReqInfo;
import com.gondola.app.model.UserInfoReq;

@Component
public class GondolaReqTransformer {

	public SubscriberList mapSubscriberInsert(SubscriberReqInfo srInfo) {
		SubscriberList sl = new SubscriberList();
		sl.setEmailaddress(srInfo.getEmail_address());
		sl.setFirstname(srInfo.getFirstname());
		sl.setIpaddress(srInfo.getIpaddress());
		sl.setMobilenumber(null != srInfo.getMobilenumber() ? srInfo.getMobilenumber() : "");
		sl.setOtpnumber("");
		sl.setOtpprefix("");
		sl.setCreateddate(new Timestamp(System.currentTimeMillis()));
		sl.setStatus("I");
		return sl;
	}
	


	public SubscriberList mapSubscriberInsertFromSignUp(UserInfoReq srInfo) {
		SubscriberList sl = new SubscriberList();
		sl.setEmailaddress(srInfo.getEmailaddress());
		sl.setFirstname(srInfo.getFirstname());
		sl.setIpaddress(srInfo.getIpaddress());
		sl.setMobilenumber(null != srInfo.getMobilenumber() ? srInfo.getMobilenumber() : "");
		sl.setOtpnumber("");
		sl.setOtpprefix("");
		sl.setCreateddate(new Timestamp(System.currentTimeMillis()));
		sl.setStatus("I");
		return sl;
	}
}
