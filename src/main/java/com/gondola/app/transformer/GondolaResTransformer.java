package com.gondola.app.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gondola.app.entities.CompanyRatings;
import com.gondola.app.entities.NaicList;
import com.gondola.app.entities.QuotesInfo;
import com.gondola.app.entities.SpeedToProtectionTest;
import com.gondola.app.model.IncomeTankCalc;
import com.gondola.app.model.ProductListDetails;
import com.gondola.app.model.QuotesReponseInfo;
import com.gondola.app.model.QuotesReqInfo;
import com.gondola.app.service.CompanyRatingsService;
import com.gondola.app.service.SpeedToProtectionService;
import com.gondola.app.ws.response.model.CompanyInfo;
import com.gondola.app.ws.response.model.LifeLink;
import com.gondola.app.ws.response.model.ModalValue;
import com.gondola.app.ws.response.model.PremiumInformation;
import com.gondola.app.ws.response.model.ProductInformation;
import com.gondola.app.ws.response.model.Ratings;
import com.gondola.app.ws.response.model.VTProduct;

@Component
public class GondolaResTransformer {

	@Autowired
	private CompanyRatingsService companyRatingsService;

	@Autowired
	private SpeedToProtectionService speedToProtectionService;

	//private static List<HealthRatings> healthRatings = new ArrayList<HealthRatings>();
	private static List<CompanyRatings> companyRatings = new ArrayList<CompanyRatings>();
	private static List<SpeedToProtectionTest> speedToProtection = new ArrayList<SpeedToProtectionTest>();
	//private static List<Conversion> conversion = new ArrayList<Conversion>();
	
	public static final Map<Integer, String> healthRatingsMap = new HashMap<>();
	
	public GondolaResTransformer() {
		healthRatingsMap.put(1, "Super Healthy");
		healthRatingsMap.put(2, "NotMappedToGL");
		healthRatingsMap.put(3, "Healthy");
		healthRatingsMap.put(4, "NotMappedToGL");
		healthRatingsMap.put(5, "Average");
		healthRatingsMap.put(0, "NotMappedToGL");
	}

	public QuotesReponseInfo mapResponseWithProductList(List<LifeLink> lifeLinkList, List<NaicList> naicList, QuotesInfo quoteReq, String isNaic) {
		QuotesReponseInfo quoteRes = null;
		Set<ProductListDetails> filterProductList = null;
		Set<ProductListDetails> productList = null;
		try {
			productList = mapProductListResObj(lifeLinkList, naicList, quoteReq,isNaic);
			quoteRes = new QuotesReponseInfo();
			quoteRes.setTotalProductList(productList);
			quoteRes.setNumberOfProductsQuoted(productList.size());
			if (null != productList) {
				filterProductList = productList.stream().distinct().collect(Collectors.toSet());
			}
			if (null != filterProductList && !filterProductList.isEmpty()) {
				quoteRes.setFilteredProductList(filterProductList);
				quoteRes.setNumberOfProductsFiltered(filterProductList.size());
			}
			/*** Income Tank Calc ***/
			if (null != quoteReq) {
				IncomeTankCalc itc = new IncomeTankCalc();
				itc.setPotentialamount(getPotentialCalc(quoteReq));
				itc.setMaxprotectamount(getMaxProtectionCalc(quoteReq));
				itc.setQuoteamount(quoteReq.getAmount());
				quoteRes.setIncomeTankCalc(itc);
			}
			quoteRes.setStatus("SUCCESS");
			quoteRes.setPageName("QUOTES_PAGE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quoteRes;
	}

	public Set<ProductListDetails> mapProductListResObj(List<LifeLink> lifeLinkList, List<NaicList> naicList, QuotesInfo quoteReq, String isNaic) {
		Set<ProductListDetails> productList = new LinkedHashSet<ProductListDetails>();
		lifeLinkList.forEach((lifeLinkInfo) -> {
			List<VTProduct> filteredProducts = null;
			if (null != lifeLinkInfo) {
				if("1".equals(isNaic)){
					filteredProducts = filteredNAICProducts(
							lifeLinkInfo.getVitalTermNet().getProducts().getProduct(), naicList, quoteReq.getStatecode());
				} else {
					filteredProducts =  lifeLinkInfo.getVitalTermNet().getProducts().getProduct();
				}
				if (null != filteredProducts) {
					filteredProducts.forEach((e) -> {
						try {
							ProductInformation productInfo = e.getScenario().getReturn().getDisplayedInformation()
									.getProductInformation();
							ProductListDetails productResObj = new ProductListDetails();
							productResObj.setProductid(e.getId());
							if (null != productInfo) {
								String productname = "";
								if (null != productInfo.getDisplayName()) {
									productname = productInfo.getDisplayName().trim();
									productname = productname.replaceAll("eApp Only", "").replaceAll("eApp", "");
								}
								productResObj.setProductname(productname);
								productResObj.setProductNAICCertified(productInfo.isNAICCertified());
								productResObj.setProductYearsLevel(productInfo.getYearsLevel());
								productResObj.setProductGuarenteedYears(productInfo.getGuaranteedPeriod().getValue());
								productResObj.setProductMaxYearsToShow(productInfo.getMaxYearsToShow());
								productResObj.setProductMinAgeIssued(productInfo.getIssueAges().getMinimum());
								productResObj.setProductMaxAgeIssued(productInfo.getIssueAges().getMaximum());
								productResObj.setProductMinFaceAmount(productInfo.getFaceAmounts().getMinimum());
								productResObj.setProductMaxFaceAmount(productInfo.getFaceAmounts().getMaximum());
								productResObj
										.setUnderWritingClass(productInfo.getUnderwritingClasses().getClassNameRan());
								productResObj
								.setUnderWritingClassId(productInfo.getUnderwritingClasses().getClassIDRan());
								if (null != productInfo.getUnderwritingClasses().getTableRatings()) {
									productResObj.setTableRatingId(productInfo.getUnderwritingClasses()
											.getTableRatings().getRatingRan().getId());
								}
								if (null != productInfo.getConversion()) {
									productResObj.setConversionname(productInfo.getConversion().getText());
									productResObj.setIsconversion(true);
								}
								if (null != productResObj.getUnderWritingClass()
										|| productResObj.getTableRatingId() > 0) {
									MaphealthRatings(productResObj);
								}
								if (null != productInfo.getAvailableRiders()
										&& !productInfo.getAvailableRiders().getRider().isEmpty()) {
									productInfo.getAvailableRiders().getRider().forEach((rider) -> {
										if (rider.getId() == 10) {
											productResObj.setWaiveraddonsid(1);
											productResObj.setWaiveraddonsname("waiver");
										} else if (rider.getId() == 11 || rider.getId() == 15) {
											productResObj.setAdbaddonsid(2);
											productResObj.setAdbaddonsname("adb");
										} else if (rider.getId() == 12) {
											productResObj.setChildaddonsid(3);
											productResObj.setChildaddonsname("child");
										}
									});
								}
							}
							PremiumInformation premiumInfo = e.getScenario().getReturn().getDisplayedInformation()
									.getPremiumInformation();
							if (null != premiumInfo) {
								if (null != premiumInfo.getPremiums()) {
									productResObj.setTotalpremiumvalues(
											premiumInfo.getPremiums().get(0).getTotalPremiums().getPremium());
								}
								List<ModalValue> premium = premiumInfo.getFirstYearModalPremiums().getPremium();
								if (null != premium) {
									premium.forEach((prem) -> {
										if (null != prem.getModal()) {
											if ("GUAR1STANNUAL".equals(prem.getModal().toUpperCase())) {
												productResObj.setGuaranteedAnnualPremiumAmount(prem.getValue());
											}
											if ("GUAR1STSEMIANNUAL".equals(prem.getModal().toUpperCase())) {
												productResObj.setGuaranteedSemiAnnualPremiumAmount(prem.getValue());
											}
											if ("GUAR1STQUARTERLY".equals(prem.getModal().toUpperCase())) {
												productResObj.setGuaranteedQuarterlyPremiumAmount(prem.getValue());
											}
											if ("GUAR1STMONTHLYBANKDRAFT".equals(prem.getModal().toUpperCase())) {
												productResObj.setGuaranteedMonthlyPremiumAmount(prem.getValue());
											}
										}
									});
								}
							}
							CompanyInfo cmInfo = e.getCompanyInfo();
							if (null != cmInfo) {
								productResObj.setCompanyName(cmInfo.getName());
								productResObj.setCompanyAddress(cmInfo.getAddress());
								productResObj.setCompanyphonenumber(cmInfo.getPhone());
								productResObj.setCompanywebsite(cmInfo.getURL());
								productResObj.setCompanyfounded(cmInfo.getFounded());
								productResObj.setIsCompanyNAICCertified(cmInfo.getNAIC().getNAICCertified());
								productResObj.setCompanyNAICCode(cmInfo.getNAIC().getCode());
								productResObj.setAllcompanyratings(cmInfo.getRatings());
								String[] companyRatingInfo = mapCompanyRating(cmInfo.getRatings());
								productResObj.setCompanyRating(companyRatingInfo[0].concat(companyRatingInfo[1]));
								productResObj.setCompanyRatingId(fetchCompanyRatingId(companyRatingInfo[0]));
								productResObj.setFaceAmount(e.getScenario().getRequest().getFaceAmount());
								mapSpeedToProtection(productResObj);
							}
							if (!compareProductsInfo(productList, productResObj)) {
								productList.add(productResObj);
							} else {
								System.out.print("Duplicate Exist -- "+productResObj.getProductid() + " Product Name" +productResObj.getProductname());
							}
						} catch (Exception e2) {
							System.out.println("product res-- " + e2);
						}
					});
				}
			}
		});
		return productList;
	}

	public boolean compareProductsInfo(Set<ProductListDetails> addedList, ProductListDetails resObj) {
		boolean isExist = false;
		List<ProductListDetails> productList = null;
		try {
			if (null != addedList && !addedList.isEmpty()) {
				productList = addedList.stream().filter(prod -> prod.getProductid() == resObj.getProductid())
						.collect(Collectors.toList());
				if (productList != null && !productList.isEmpty()) {
					for (ProductListDetails pld : productList) {
						if (pld.getGuaranteedMonthlyPremiumAmount().equals(resObj.getGuaranteedMonthlyPremiumAmount())
								&& pld.getUnderWritingClass().equals(resObj.getUnderWritingClass())) {
							isExist = true;
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return isExist;
	}

	public List<VTProduct> filteredNAICProducts(List<VTProduct> filterProduct, List<NaicList> naicList,String stateCode) {
		int status = "NY".equals(stateCode) ? 2 : 1;
		if (null != filterProduct) {
			try {
				filterProduct = filterProduct.stream().filter(product -> naicList.stream()
						.filter(naic -> Objects.nonNull(product.getCompanyInfo()))
						.filter(naic -> Objects.nonNull(product.getCompanyInfo().getNAIC().getCode()))
						.anyMatch(naic -> product.getCompanyInfo().getNAIC().getCode().equals(naic.getNaicCode()) && naic.getStatus() == status))
						.collect(Collectors.toList());
			} catch (Exception e) {
				System.out.println("test -- " + e);
			}
		}
		return filterProduct;
	}

	public String[] mapCompanyRating(Ratings Rating) {
		String rating = "";
		String companyName = "";
		try {
			if (null != Rating) {
				if (null != Rating.getAMBest() && !("").equals(Rating.getAMBest().trim().replaceAll("\n", ""))) {
					rating = Rating.getAMBest().substring(0, Rating.getAMBest().indexOf("(")).trim();
					companyName = " (AMBest)";
				} else if (null != Rating.getSandP() && !("").equals(Rating.getSandP().trim().replaceAll("\n", ""))) {
					rating = Rating.getSandP().substring(0, Rating.getSandP().indexOf("(")).trim();
					companyName = " (SandP)";
				} else if (null != Rating.getFitch() && !("").equals(Rating.getFitch().trim().replaceAll("\n", ""))) {
					rating = Rating.getFitch().substring(0, Rating.getFitch().indexOf("(")).trim();
					companyName = " (Fitch)";
				} else if (null != Rating.getMoodys() && !("").equals(Rating.getMoodys().trim().replaceAll("\n", ""))) {
					rating = Rating.getMoodys().substring(0, Rating.getMoodys().indexOf("(")).trim();
					companyName = " (Moodys)";
				} else {
					rating = "No";
					companyName = " Ratings";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String[] { rating, companyName };
	}

	/*public void insertConversion(ProductListDetails resObj) {
		try {
			if (conversion.size() == 0) {
				conversion = conversionService.fetchConversions();
			}
			if (conversion.size() > 0 && !resObj.getConversionname().isEmpty()) {
				Conversion conversionfilter = conversion.stream()
						.filter(convers -> resObj.getCompanyName().trim().equals(convers.getCareername().trim())
								&& resObj.getConversionname().trim().equals(convers.getConversionname().trim())
								&& convers.getProductname().trim().equals(convers.getProductname().trim()))
						.findAny().orElse(null);
				if (null != conversionfilter) {
					Conversion cr = new Conversion();
					cr.setCareername(resObj.getCompanyName());
					cr.setConversionname(resObj.getConversionname());
					cr.setProductid((int) resObj.getProductid());
					cr.setProductname(resObj.getProductname());
					conversionService.insertConversion(cr);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}*/

	public void MaphealthRatings(ProductListDetails productResObj) {
		///HealthRatings fetchRatings = null;
		try {
			/*if (healthRatings.size() == 0) {
				healthRatings = healthRatingsService.fetchHealthRatingsList();
			}
			if (healthRatings.size() > 0 && null != productResObj.getUnderWritingClass()) {
				fetchRatings = healthRatings.stream().filter(healthrating -> productResObj.getUnderWritingClass().trim()
						.equals(healthrating.getUnderwritingclass().trim())).findAny().orElse(null);
			}
			insertUnderWriting(productResObj.getUnderWritingClass(), productResObj.getUnderWritingClassId());*/
			Integer uwClassId = productResObj.getUnderWritingClassId();
			if (null != productResObj.getTableRatingId() && productResObj.getTableRatingId() == 2) {
				productResObj.setHealthRatingId(4);
				productResObj.setHealthRatingName("Fair");
			} else if (null != productResObj.getTableRatingId() && productResObj.getTableRatingId() == 4) {
				productResObj.setHealthRatingId(5);
				productResObj.setHealthRatingName("Poor");
			} else if (null != uwClassId) {
				productResObj.setHealthRatingId(uwClassId == 1 ? uwClassId : uwClassId == 3 ? 2 : uwClassId == 5 ? 3 : 0);
				productResObj.setHealthRatingName(healthRatingsMap.get(uwClassId));
			} else {
				productResObj.setHealthRatingId(0);
				productResObj.setHealthRatingName("NotMappedToGL");
				/*if (null == fetchRatings && null != productResObj.getUnderWritingClass() && null != healthRatings) {
					insertHealthRating(productResObj.getUnderWritingClass().trim(), healthRatings.size());
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception in Map Health Ratings method -- " + e);
		}
	}

	/*public void insertHealthRating(String HealthRating, int listSize) {
		try {
			HealthRatings hr = new HealthRatings();
			hr.setId(listSize + 1);
			hr.setUnderwritingclass(HealthRating.trim());
			hr.setStatus(0);
			HealthRatings hr1 = healthRatingsService.insertHealthRatingsList(hr);
			if (null != hr1) {
				healthRatings = new ArrayList<HealthRatings>();
				healthRatings = healthRatingsService.fetchHealthRatingsList();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("exception in insert health ratings -- " + e);
		}
	}*/
	

	/*public void insertUnderWriting(String classname, int classid) {
		try {
			UnderWritingClass uc = new UnderWritingClass();
			uc.setUnderwritingclassid(classid);
			uc.setUnderwritingclassname(classname);
			underWritingRepository.save(uc);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("exception in insert health ratings -- " + e);
		}
	}*/

	public int fetchCompanyRatingId(String ratingName) {
		int companyRatingid = 0;
		try {
			if (!"No".equals(ratingName)) {
				if (companyRatings.size() == 0) {
					companyRatings = companyRatingsService.fetchCompanyRatings();
				}
				if (companyRatings.size() > 0 && null != ratingName) {
					CompanyRatings compRating = companyRatings.stream()
							.filter(compRat -> ratingName.trim().equals(compRat.getRatingname().trim())).findAny()
							.orElse(null);
					if (null != compRating) {
						companyRatingid = compRating.getId();
					} else {
						CompanyRatings cr = new CompanyRatings();
						cr.setRatingname(ratingName);
						cr.setId(companyRatings.size() + 1);
						CompanyRatings rating = companyRatingsService.insertCompanyRatings(cr);
						if (null != rating) {
							companyRatingid = rating.getId();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("exception in fetch CompanyRatingId method -- " + e);
		}
		return companyRatingid;
	}

	public void mapSpeedToProtection(ProductListDetails productResObj) {
		try {
			if (speedToProtection.size() == 0) {
				speedToProtection = speedToProtectionService.fetchSpeedToProtection();
			}
			if (speedToProtection.size() > 0 && null != productResObj.getCompanyNAICCode()) {
				SpeedToProtectionTest speedProtect = speedToProtection
						.stream().filter(speedToProtect -> Integer
								.parseInt(productResObj.getCompanyNAICCode()) == speedToProtect.getNaiccode())
						.findAny().orElse(null);
				if (null != speedProtect) {
					productResObj.setSpeedToProtectionid(speedProtect.getSpeedtoprotectid());
					productResObj.setSpeedToProtectionname(speedProtect.getSpeedtoprotectionname());
				} else {
					productResObj.setSpeedToProtectionid(0);
					productResObj.setSpeedToProtectionname("Not Provided");
				}
			} else {
				productResObj.setSpeedToProtectionid(0);
				productResObj.setSpeedToProtectionname("NAIC Code Empty");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("exception in map Speed To Protection method -- " + e);
		}
	}

	private long getPotentialCalc(QuotesInfo q) {
		long potential = 0;
		try {
			if (null != q && q.getIsWorking().equals("1") && q.getAge() > 0
					&& q.getExpectedretirementage() >= q.getAge()) {
				int calcage = q.getExpectedretirementage() - q.getAge();
				long income = potential = q.getAnnualincome();
				long annualincome = 0;
				for (int i = 0; i < calcage; i++) {
					if (i != 0) {
						annualincome = Math.round(income + Math.round(potential * 0.03));
						potential = Math.round(potential + annualincome);
					}
				}
			} else if (null != q && q.getIsWorking().equals("2") && q.getEstAssets() > 0) {
				potential = q.getEstAssets();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return potential;
	}

	private long getMaxProtectionCalc(QuotesInfo q) {
		long maxProtect = 0;
		try {
			if (null != q && q.getIsWorking().equals("2") && q.getEstAssets() > 0) {
				maxProtect = q.getEstAssets() / 2;
			} else if (null != q && q.getAge() > 0 && q.getIsWorking().equals("1")) {
				if (q.getAge() >= 18 && q.getAge() <= 40) {
					maxProtect = 30 * q.getAnnualincome();
				} else if (q.getAge() >= 41 && q.getAge() <= 50) {
					maxProtect = 25 * q.getAnnualincome();
				} else if (q.getAge() >= 51 && q.getAge() <= 55) {
					maxProtect = 20 * q.getAnnualincome();
				} else if (q.getAge() >= 56 && q.getAge() <= 65) {
					maxProtect = 15 * q.getAnnualincome();
				} else if (q.getAge() >= 66 && q.getAge() <= 70) {
					maxProtect = 10 * q.getAnnualincome();
				} else if (q.getAge() >= 71 && q.getAge() <= 74) {
					maxProtect = 5 * q.getAnnualincome();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maxProtect;
	}
	
	public QuotesInfo mapQuotes(QuotesReqInfo r) {
		QuotesInfo q = new QuotesInfo();
		q.setSubscriberid(r.getSubscriberid());
		q.setEmailaddress(r.getEmailaddress());
		q.setAge(r.getAge());
		q.setAmount(r.getAmount());
		q.setAnnualincome(r.getAnnualincome());
		q.setAdvancefilter(r.isAdvancefilter());
		q.setEstAssets(r.getEstAssets());
		q.setExpectedretirementage(r.getExpectedretirementage());
		q.setGender(r.getGender());
		q.setIpaddress(r.getIpaddress());
		q.setIssmoker(r.getIssmoker());
		q.setIsWorking(r.getIsWorking());
		q.setStatecode(r.getStatecode());
		q.setYears(r.getYears());
		if(r.getAddonslist() != null){
			q.setAddonslist(r.getAddonslist().toString());			
		}
		return q;
	}
}