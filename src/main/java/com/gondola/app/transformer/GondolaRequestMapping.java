package com.gondola.app.transformer;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gondola.app.model.QuotesReqInfo;
import com.gondola.app.service.QuotesReponseService;
import com.gondola.app.ws.response.model.LifeLink;

@Component
public class GondolaRequestMapping {

	@Autowired
	private QuotesReponseService quotesReponseService;

	public String createXMLRequestInfo(QuotesReqInfo quotesInfo, int call) {
		String requestXML = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(
					"<LifeLink xmlns='urn:lifelink-schema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='urn:lifelink-schema llXML.xsd'>");
			sb.append("<LL>");
			sb.append("<UserName>gondolaUser</UserName>");
			sb.append("<UserPassword>Gondola#1</UserPassword>");
			sb.append("<InterfaceType>NoGUI</InterfaceType>");
			sb.append("<OutputType>XML</OutputType>");
			sb.append("<Logout>false</Logout>");
			sb.append("<Tool>");
			sb.append("<Name>VitalTerm_Net</Name>");
			sb.append("</Tool>");
			sb.append("</LL>");
			sb.append("<Client>");
			sb.append("<Applicant>");
			sb.append("<Age>" + quotesInfo.getAge() + "</Age>");
			sb.append("<Sex>" + quotesInfo.getGender() + "</Sex>");
			sb.append("</Applicant>");
			sb.append("<StateOfIssue>" + quotesInfo.getStatecode() + "</StateOfIssue>");
			sb.append("</Client>");
			sb.append("<VitalTermNet>");
			sb.append("<Products>");
			sb.append("<ProductOptions>");
			sb.append("<BreakDownPremiums>0</BreakDownPremiums>");
			sb.append("<CalcToPenny>1</CalcToPenny>");
			sb.append("<DisplayProductClasses>1</DisplayProductClasses>");
			sb.append("<DisplayProductTableRatings>1</DisplayProductTableRatings>");
			sb.append("<GetCompanyInfo>1</GetCompanyInfo>");
			sb.append("<ShowCurrentRates>0</ShowCurrentRates>");
			sb.append("<ShowRemovedProducts>0</ShowRemovedProducts>");
			sb.append("<ShowProdInfoOnRemoval>1</ShowProdInfoOnRemoval>");
			sb.append("<ShowProdInfoOnly>0</ShowProdInfoOnly>");
			sb.append("<ShowPremiumsOnly>0</ShowPremiumsOnly>");
			sb.append("<Calc1stYearModal>1</Calc1stYearModal>");
			if (quotesInfo.isAdvancefilter()) {
				sb.append("<ShowAvailableRiders>1</ShowAvailableRiders>");
			}
			sb.append("</ProductOptions>");
			sb.append("<ProductLists>");
			sb.append("<YearsLevel>" + quotesInfo.getYears() + "</YearsLevel>");
			sb.append("</ProductLists>");
			sb.append("<ProductScenarios>");
			if (call == 1) {
				if ("1".equals(quotesInfo.getIssmoker())) {
					sb.append("<Scenario>");
					sb.append("<FaceAmount>" + quotesInfo.getAmount() + "</FaceAmount>");
					sb.append("<PaymentMode>0</PaymentMode>");
					sb.append("<UnderwritingClassInfo type='VT'>");
					sb.append("<VTClassID>1</VTClassID>");
					sb.append("<VTSmokingStatus>" + quotesInfo.getIssmoker() + "</VTSmokingStatus>");
					sb.append("</UnderwritingClassInfo>");
					if (quotesInfo.isAdvancefilter() && null != quotesInfo.getAddonslist()) {
						sb.append(getRidersMapping(quotesInfo));
					}
					sb.append("</Scenario>");
				}
				sb.append("<Scenario>");
				sb.append("<FaceAmount>" + quotesInfo.getAmount() + "</FaceAmount>");
				sb.append("<PaymentMode>0</PaymentMode>");
				sb.append("<UnderwritingClassInfo type='VT'>");
				sb.append("<VTClassID>3</VTClassID>");
				sb.append("<VTSmokingStatus>" + quotesInfo.getIssmoker() + "</VTSmokingStatus>");
				sb.append("</UnderwritingClassInfo>");
				if (quotesInfo.isAdvancefilter() && null != quotesInfo.getAddonslist()) {
					sb.append(getRidersMapping(quotesInfo));
				}
				sb.append("</Scenario>");
				sb.append("<Scenario>");
				sb.append("<FaceAmount>" + quotesInfo.getAmount() + "</FaceAmount>");
				sb.append("<PaymentMode>0</PaymentMode>");
				sb.append("<UnderwritingClassInfo type='VT'>");
				sb.append("<VTClassID>5</VTClassID>");
				sb.append("<VTSmokingStatus>" + quotesInfo.getIssmoker() + "</VTSmokingStatus>");
				sb.append("</UnderwritingClassInfo>");
				if (quotesInfo.isAdvancefilter() && null != quotesInfo.getAddonslist()) {
					sb.append(getRidersMapping(quotesInfo));
				}
				sb.append("</Scenario>");
			}
			if (call == 2) {
				sb.append("<Scenario>");
				sb.append("<FaceAmount>" + quotesInfo.getAmount() + "</FaceAmount>");
				sb.append("<PaymentMode>0</PaymentMode>");
				sb.append("<UnderwritingClassInfo type='VT'>");
				sb.append("<TableRating>2</TableRating>");
				sb.append("<VTClassID>5</VTClassID>");
				sb.append("<VTSmokingStatus>" + quotesInfo.getIssmoker() + "</VTSmokingStatus>");
				sb.append("</UnderwritingClassInfo>");
				if (quotesInfo.isAdvancefilter() && null != quotesInfo.getAddonslist()) {
					sb.append(getRidersMapping(quotesInfo));
				}
				sb.append("</Scenario>");
				sb.append("<Scenario>");
				sb.append("<FaceAmount>" + quotesInfo.getAmount() + "</FaceAmount>");
				sb.append("<PaymentMode>0</PaymentMode>");
				sb.append("<UnderwritingClassInfo type='VT'>");
				sb.append("<TableRating>4</TableRating>");
				sb.append("<VTClassID>5</VTClassID>");
				sb.append("<VTSmokingStatus>" + quotesInfo.getIssmoker() + "</VTSmokingStatus>");
				sb.append("</UnderwritingClassInfo>");
				if (quotesInfo.isAdvancefilter() && null != quotesInfo.getAddonslist()) {
					sb.append(getRidersMapping(quotesInfo));
				}
				sb.append("</Scenario>");
			}
			sb.append("</ProductScenarios>");
			sb.append("</Products>");
			sb.append("</VitalTermNet>");
			sb.append("</LifeLink>");
			requestXML = sb.toString();
			System.out.println(requestXML);
		} catch (

		Exception e) {
		}
		return requestXML;
	}

	public List<LifeLink> getLifeLinkList(QuotesReqInfo quotesInfo) throws InterruptedException, ExecutionException {
		List<LifeLink> lifeLinkInfo = null;
		if (null != quotesInfo) {
			lifeLinkInfo = quotesReponseService.getLifeLinkList(createXMLRequestInfo(quotesInfo, 1),
					createXMLRequestInfo(quotesInfo, 2));
		}
		return lifeLinkInfo;
	}

	public String getRidersMapping(QuotesReqInfo quotesInfo) {
		StringBuilder ridersb = new StringBuilder();
		List<String> riderType = quotesInfo.getAddonslist();
		if (riderType.size() > 0) {
			ridersb.append("<Riders>");
			riderType.forEach((ele) -> {
				if ("Waiver".equals(ele)) {
					ridersb.append("<Rider name='Waiver' id='10'>");
					ridersb.append("</Rider>");
				}
				if ("ADB".equals(ele)) {
					ridersb.append("<Rider name='ADB' id='11'>");
					/*
					 * sb.append("<RiderRequestInfo>");
					 * sb.append("<ADBFaceAmount>0</ADBFaceAmount>");
					 * sb.append("<UseADBMaxFace>1</UseADBMaxFace>");
					 * sb.append("</RiderRequestInfo>");
					 */
					ridersb.append("</Rider>");
				}
				if ("Child".equals(ele)) {
					ridersb.append("<Rider name='Child' id='12'>");
					/*
					 * sb.append("<RiderRequestInfo>"); sb.append("<UnitAmount>0</UnitAmount>");
					 * sb.append("<UseMaxUnits>true</UseMaxUnits>");
					 * sb.append("<ChildAgeYoungest value='1' type='Years'/>");
					 * sb.append("</RiderRequestInfo>");
					 */
					ridersb.append("</Rider>");
				}
			});
			ridersb.append("</Riders>");
		}
		return ridersb.toString();
	}

}