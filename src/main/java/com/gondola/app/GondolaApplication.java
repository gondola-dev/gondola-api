package com.gondola.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GondolaApplication extends SpringBootServletInitializer {
	
	private static Logger logger = LoggerFactory.getLogger(GondolaApplication.class);
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(GondolaApplication.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(GondolaApplication.class, args);
		logger.info("*** -> Gondola Application Started <- ***");
	}

}
