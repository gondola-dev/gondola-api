package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.UserInfoList;

public interface UserInfoListService {
	
	List<UserInfoList> fetchUserList();

	UserInfoList insertUserList(UserInfoList cr);
	
	public UserInfoList fetchUserInfoByEmail(String emailaddress);
}
