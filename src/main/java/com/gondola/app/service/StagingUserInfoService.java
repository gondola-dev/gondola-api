package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.StagingUserInfo;

public interface StagingUserInfoService {
	
	List<StagingUserInfo> fetchStagingUserInfo();

	StagingUserInfo insertStagingUserInfo(StagingUserInfo cr);

	StagingUserInfo fetchStagingUserInfoByIpaddress(String ipaddress);
	
}
