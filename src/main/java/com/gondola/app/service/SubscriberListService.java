package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.SubscriberList;

public interface SubscriberListService {
	
	List<SubscriberList> fetchSubscriberList();
	
	SubscriberList fetchSubscriberByEmail(String email_address);

	SubscriberList insertSubscriberList(SubscriberList cr);

	SubscriberList fetchSubscriberByIpaddress(String ipaddress);
	
}
