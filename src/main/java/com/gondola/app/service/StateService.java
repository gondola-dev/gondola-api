package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.States;

public interface StateService {
    List<States> fetchStateList();
}
