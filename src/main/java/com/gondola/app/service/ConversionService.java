package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.Conversion;

public interface ConversionService {
	List<Conversion> fetchConversions();
	Conversion insertConversion(Conversion cr);
}
