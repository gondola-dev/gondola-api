package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.CompanyRatings;

public interface CompanyRatingsService {
	
	List<CompanyRatings> fetchCompanyRatings();

	CompanyRatings insertCompanyRatings(CompanyRatings cr); 
}
