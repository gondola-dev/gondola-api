package com.gondola.app.service;

import java.util.List;

import com.gondola.app.ws.response.model.LifeLink;

public interface QuotesReponseService {
	
	public List<LifeLink> getLifeLinkList(String req1, String req2);
}
