package com.gondola.app.service;

import org.jsoup.internal.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.SubscriberList;
import com.gondola.app.model.EmailDTO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Description(value = "Service responsible for handling OTP related functionality.")
@Service
public class OtpService {

	private final Logger LOGGER = LoggerFactory.getLogger(OtpService.class);

	@Autowired
	private OtpGenerator otpGenerator;

	@Autowired
	private EmailService emailService;

	@Autowired
	private SubscriberListService subscriberListService;

	/**
	 * Constructor dependency injector
	 * 
	 * @param otpGenerator - otpGenerator dependency
	 * @param emailService - email service dependency
	 * @param userService  - user service dependency
	 */
	public OtpService(OtpGenerator otpGenerator, EmailService emailService) {
		this.otpGenerator = otpGenerator;
		this.emailService = emailService;
	}

	/**
	 * Method for generate OTP number
	 *
	 * @param key - provided key (username in this case)
	 * @return boolean value (true|false)
	 */
	public Boolean generateOtp(String key, boolean isResend) {
		boolean emailStatus = false;
		try {
			if(isResend){
				otpGenerator.clearOTPFromCache(key);
			}
			// generate otp
			Integer otpValue = otpGenerator.generateOTP(key);
			if (otpValue == -1) {
				LOGGER.error("OTP generator is not working...");
				return false;
			}

			LOGGER.info("Generated OTP: {}", otpValue);

			List<String> recipients = new ArrayList<>();
			recipients.add(key);

			// generate emailDTO object
			EmailDTO emailDTO = new EmailDTO();
			emailDTO.setSubject("Gondola Life Beta Registration OTP");
			emailDTO.setBody("Thanks for Registering in Gondola Life Beta Launch. The OTP is "+otpValue+" and its valid for 5 minutes.");
			emailDTO.setHtml(true);
			emailDTO.setRecipients(recipients);
			emailStatus = emailService.sendSimpleMessage(emailDTO);
			if (emailStatus) {
				try {
					SubscriberList subscriberInfo = subscriberListService.fetchSubscriberByEmail(key);
					subscriberInfo.setModifieddate(new Timestamp(System.currentTimeMillis()));
					subscriberInfo.setStatus("S");
					subscriberInfo.setOtpnumber(String.valueOf(otpValue));
					if(!StringUtil.isBlank(subscriberInfo.getOtpprefix())){
						String otpVal = String.valueOf(Integer.parseInt(subscriberInfo.getOtpprefix()) + 1);
						subscriberInfo.setOtpprefix(otpVal);
					} else {
						subscriberInfo.setOtpprefix("1");	
					}
					subscriberListService.insertSubscriberList(subscriberInfo);
				} catch (Exception e) {
					LOGGER.error("Error while updating the data " + e.getMessage());
				}
			}

		} catch (Exception e) {
			LOGGER.error("Error while sending an email" + e.getMessage());
		}
		// send generated e-mail
		return emailStatus;
	}

	/**
	 * Method for validating provided OTP
	 *
	 * @param key       - provided key
	 * @param otpNumber - provided OTP number
	 * @return boolean value (true|false)
	 */
	public Boolean validateOTP(String key, Integer otpNumber) {
		// get OTP from cache
		Integer cacheOTP = otpGenerator.getOPTByKey(key);
		if (cacheOTP != null && cacheOTP.equals(otpNumber)) {
			otpGenerator.clearOTPFromCache(key);
			return true;
		}
		return false;
	}
	
	public String getBody(Integer otpValue) {
		StringBuilder sb = new StringBuilder();
		// message contains HTML markups
		sb.append("<i>Hi User,</i><br>");
		sb.append("<b>Thanks for Registering in Gondola Life Beta Launch.</b><br>");
		sb.append("The OTP is <b>"+otpValue+"</b> and its valid for 5 minutes.<br>");
		return sb.toString();
	}
}
