package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.QuotesInfo;

public interface GondolaService {
	
	List<QuotesInfo> fetchQuotesInfo();

	QuotesInfo insertQuotesInfo(QuotesInfo cr);

	List<QuotesInfo> fetchQuotesInfoByIpaddress(String ipaddress);
	
}
