package com.gondola.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.InsurerList;
import com.gondola.app.repository.InsurerListRepository;
import com.gondola.app.service.InsurerListService;

@Service
public class InsurerListServiceImpl implements InsurerListService{

	@Autowired
	private InsurerListRepository insurerListRepository;
	
	@Override
	public List<InsurerList> fetchInsurerList() {
		// TODO Auto-generated method stub
		List<InsurerList> insurerList = insurerListRepository.findAll();
		return insurerList;
	}
}
