package com.gondola.app.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.QuotesInfo;
import com.gondola.app.repository.GondolaRepository;
import com.gondola.app.service.GondolaService;

@Service
public class GondolaServiceImpl implements GondolaService{
 
	@Autowired
	private GondolaRepository gondolaRepository;
	
	@Override
	public List<QuotesInfo> fetchQuotesInfo() {
		// TODO Auto-generated method stub
		List<QuotesInfo> quotesInfoList = gondolaRepository.findAll();
		return quotesInfoList;
	}

	@Override
	public QuotesInfo insertQuotesInfo(QuotesInfo cr) {
		// TODO Auto-generated method stub
		QuotesInfo quotesInfoList = gondolaRepository.save(cr);
		return quotesInfoList;
	}
	
	@Override
	public List<QuotesInfo> fetchQuotesInfoByIpaddress(String ipaddress) {
		List<QuotesInfo> quoteList = new ArrayList<>();
		Optional<List<QuotesInfo>> quotesInfoList = gondolaRepository.findByipaddress(ipaddress);
		if (quotesInfoList.isPresent()) {
            if(!quotesInfoList.get().isEmpty()) {
            	quoteList.addAll(quotesInfoList.get());
            }
        }
        return quoteList;
	}
}
