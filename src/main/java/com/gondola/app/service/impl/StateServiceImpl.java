package com.gondola.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.States;
import com.gondola.app.repository.StateRepository;
import com.gondola.app.service.StateService;

@Service
public class StateServiceImpl implements StateService{

	@Autowired
	private StateRepository stateRepository;
	
	@Override
	public List<States> fetchStateList() {
		// TODO Auto-generated method stub
		List<States> stateList = stateRepository.findAll();
		return stateList;
	}
  
}
