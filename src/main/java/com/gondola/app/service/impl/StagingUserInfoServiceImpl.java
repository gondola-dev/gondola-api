package com.gondola.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.StagingUserInfo;
import com.gondola.app.repository.StagingUserRepository;
import com.gondola.app.service.StagingUserInfoService;

@Service
public class StagingUserInfoServiceImpl implements StagingUserInfoService{
 
	@Autowired
	private StagingUserRepository stagingUserRepository;
	
	@Override
	public List<StagingUserInfo> fetchStagingUserInfo() {
		// TODO Auto-generated method stub
		List<StagingUserInfo> stagingUserInfo = stagingUserRepository.findAll();
		return stagingUserInfo;
	}

	@Override
	public StagingUserInfo insertStagingUserInfo(StagingUserInfo cr) {
		// TODO Auto-generated method stub
		StagingUserInfo stagingUserInfoList = stagingUserRepository.save(cr);
		return stagingUserInfoList;
	}
	

	@Override
	public StagingUserInfo fetchStagingUserInfoByIpaddress(String ipaddress) {
		Optional<StagingUserInfo> stagingUserInfo = stagingUserRepository.findByipaddress(ipaddress);
		if (stagingUserInfo.isPresent()) {
            return stagingUserInfo.get();
        }
        return null;
	}
}
