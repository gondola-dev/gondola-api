package com.gondola.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.SpeedToProtectionTest;
import com.gondola.app.repository.SpeedToProtectionRepository;
import com.gondola.app.service.SpeedToProtectionService;

@Service
public class SpeedToProtectionServiceImpl implements SpeedToProtectionService{

	@Autowired
	private SpeedToProtectionRepository speedToProtectionRepository;
	
	@Override
	public List<SpeedToProtectionTest> fetchSpeedToProtection() {
		// TODO Auto-generated method stub
		List<SpeedToProtectionTest> speedToProtection = speedToProtectionRepository.findAll();
		return speedToProtection;
	}

	@Override
	public SpeedToProtectionTest insertSpeedToProtection(SpeedToProtectionTest cr) {
		// TODO Auto-generated method stub
		SpeedToProtectionTest speedToProtection = speedToProtectionRepository.save(cr);
		return speedToProtection;
	}
}
