package com.gondola.app.service.impl;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.gondola.app.ws.response.model.LifeLink;

@Service
public class QuotesServiceClient {

	private static final Logger logger = LoggerFactory.getLogger(QuotesServiceClient.class);

	@Value("${vitalquote.app.ws.endpoint}")
	private String quoteURL;

	RestTemplate restTemplate = new RestTemplate();
	
	public LifeLink getLifeLinkInfo(String requestObj) {
		LifeLink lifeLinkRes = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("inXML", requestObj);
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers);
			long startTime = System.currentTimeMillis();
			logger.info("Quote URL -- "+quoteURL);
			ResponseEntity<String> response = restTemplate.postForEntity(quoteURL, request, String.class);
			long endTime = System.currentTimeMillis();
			logger.info("Total Time Taken for API Call " + (endTime - startTime) + " milliseconds");
			JAXBElement<LifeLink> resObjLifeLink = convertObject(response.getBody().toString());
			if (null != resObjLifeLink) {
				lifeLinkRes = resObjLifeLink.getValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error --> " + e);
		}
		return lifeLinkRes;
	}

	private JAXBElement<LifeLink> convertObject(String response) {
		JAXBElement<LifeLink> resObj = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(LifeLink.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			resObj = unmarshaller.unmarshal(new StreamSource(new StringReader(response)), LifeLink.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return resObj;
	}

}
