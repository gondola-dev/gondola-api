package com.gondola.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.SubscriberList;
import com.gondola.app.repository.SubscriberListRepository;
import com.gondola.app.service.SubscriberListService;

@Service
public class SubscriberListServiceImpl implements SubscriberListService{
 
	@Autowired
	private SubscriberListRepository subscriberListRepository;
	
	@Override
	public List<SubscriberList> fetchSubscriberList() {
		// TODO Auto-generated method stub
		List<SubscriberList> subscriberList = subscriberListRepository.findAll();
		return subscriberList;
	}

	@Override
	public SubscriberList insertSubscriberList(SubscriberList cr) {
		// TODO Auto-generated method stub
		SubscriberList subscriberList = subscriberListRepository.save(cr);
		return subscriberList;
	}
	
	@Override
	public SubscriberList fetchSubscriberByEmail(String emailaddress) {
		Optional<SubscriberList> subscriberList = subscriberListRepository.findByemailaddress(emailaddress);
		if (subscriberList.isPresent()) {
            return subscriberList.get();
        }
        return null;
	}
	

	@Override
	public SubscriberList fetchSubscriberByIpaddress(String ipaddress) {
		List<SubscriberList> subscriberList = subscriberListRepository.findByipaddress(ipaddress);
		if (!subscriberList.isEmpty()) {
            return subscriberList.get(0);
        }
        return null;
	}
}
