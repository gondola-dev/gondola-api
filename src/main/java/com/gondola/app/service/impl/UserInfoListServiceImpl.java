package com.gondola.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.UserInfoList;
import com.gondola.app.repository.UserInfoListRepository;
import com.gondola.app.service.UserInfoListService;

@Service
public class UserInfoListServiceImpl implements UserInfoListService{

	@Autowired
	private UserInfoListRepository userListRepository;
	
	@Override
	public List<UserInfoList> fetchUserList() {
		// TODO Auto-generated method stub
		List<UserInfoList> userList = userListRepository.findAll();
		return userList;
	}

	@Override
	public UserInfoList insertUserList(UserInfoList cr) {
		// TODO Auto-generated method stub
		UserInfoList userList = userListRepository.save(cr);
		return userList;
	}
	

	@Override
	public UserInfoList fetchUserInfoByEmail(String emailaddress) {
		Optional<UserInfoList> userInfoList = userListRepository.findByemailaddress(emailaddress);
		if (userInfoList.isPresent()) {
            return userInfoList.get();
        }
        return null;
	}
}
