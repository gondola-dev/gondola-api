package com.gondola.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.CompanyRatings;
import com.gondola.app.repository.CompanyRatingsRepository;
import com.gondola.app.service.CompanyRatingsService;

@Service
public class CompanyRatingsServiceImpl implements CompanyRatingsService{

	@Autowired
	private CompanyRatingsRepository companyRatingsRepository;
	
	@Override
	public List<CompanyRatings> fetchCompanyRatings() {
		// TODO Auto-generated method stub
		List<CompanyRatings> companyRatings = companyRatingsRepository.findAll();
		return companyRatings;
	}

	@Override
	public CompanyRatings insertCompanyRatings(CompanyRatings cr) {
		// TODO Auto-generated method stub
		CompanyRatings companyRatings = companyRatingsRepository.save(cr);
		return companyRatings;
	}
}
