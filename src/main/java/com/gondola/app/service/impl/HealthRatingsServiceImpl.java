package com.gondola.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.HealthRatings;
import com.gondola.app.repository.HealthRatingsRepository;
import com.gondola.app.service.HealthRatingsService;

@Service
public class HealthRatingsServiceImpl implements HealthRatingsService{

	@Autowired
	private HealthRatingsRepository healthRatingsRepository;
	
	@Override
	public List<HealthRatings> fetchHealthRatingsList() {
		// TODO Auto-generated method stub
		List<HealthRatings> healthRatings = healthRatingsRepository.findAll();
		return healthRatings;
	}

	@Override
	public HealthRatings insertHealthRatingsList(HealthRatings hr) {
		// TODO Auto-generated method stub
		HealthRatings healthRatings = healthRatingsRepository.save(hr);
		return healthRatings;
	}
}
