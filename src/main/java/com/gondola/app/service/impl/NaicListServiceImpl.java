package com.gondola.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.NaicList;
import com.gondola.app.repository.NaicRepository;
import com.gondola.app.service.NaicListService;

@Service
public class NaicListServiceImpl implements NaicListService{

	@Autowired
	private NaicRepository naicRespository;
	
	@Override
	public List<NaicList> getNaicList() {
		// TODO Auto-generated method stub
		return naicRespository.findAll();
	}
}
