package com.gondola.app.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.gondola.app.service.QuotesReponseService;
import com.gondola.app.ws.response.model.LifeLink;

@Service
public class QuotesResponseServiceImpl implements QuotesReponseService{

	private static final Logger logger = LoggerFactory.getLogger(QuotesResponseServiceImpl.class);

	@Value("${vitalquote.app.ws.endpoint}")
	private String quoteURL;
	
	@Autowired
	private QuotesServiceClient quotesService;
	
	public List<LifeLink> lifeLinkList = null;

	public List<LifeLink> getLifeLinkList(String req1, String req2) {
		List<LifeLink> lifeLinkRes = new ArrayList<>();
		List<Future<?>> futures = new ArrayList<Future<?>>();
		long startTime = System.currentTimeMillis();
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		LifeLinkThread t1 = new LifeLinkThread(req1);
		LifeLinkThread t2 = new LifeLinkThread(req2);
		Future<?> f1 = executorService.submit(t1);
	    futures.add(f1);
		Future<?> f2 = executorService.submit(t2);
	    futures.add(f2);
		executorService.shutdown();
		while(!executorService.isTerminated()) {
		}
		boolean allDone = false;
		for(Future<?> future : futures){
		    allDone = future.isDone(); // check if future is done
		}
		long endTime = System.currentTimeMillis();
		logger.info("Total Time Taken for Response Return " + (endTime - startTime) + " milliseconds");
		if(allDone){
			lifeLinkRes.addAll(lifeLinkList);
		}
		return lifeLinkRes;
	}

	class LifeLinkThread implements Runnable {
		String requestObj = null;

		LifeLinkThread(String requestObj) {
			this.requestObj = requestObj;
		}

		@Override
		public void run() {
			lifeLinkList = new ArrayList<>();
			LifeLink resObj = quotesService.getLifeLinkInfo(requestObj);
			if(null != resObj){
				lifeLinkList.add(resObj);				
			}
		}

	}
}
