package com.gondola.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gondola.app.entities.Conversion;
import com.gondola.app.repository.ConversionRepository;
import com.gondola.app.service.ConversionService;

@Service
public class ConversionServiceImpl implements ConversionService{

	@Autowired
	private ConversionRepository conversionRepository;
	
	@Override
	public List<Conversion> fetchConversions(){
		List<Conversion> conversions = conversionRepository.findAll();
		return conversions;
	}

	@Override
	public Conversion insertConversion(Conversion cr) {
		// TODO Auto-generated method stub
		Conversion conversion = conversionRepository.save(cr);
		return conversion;
	}
}
