package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.InsurerList;

public interface InsurerListService {
	
	List<InsurerList> fetchInsurerList();

}
