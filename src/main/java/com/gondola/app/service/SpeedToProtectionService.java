package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.SpeedToProtectionTest;

public interface SpeedToProtectionService {
	
	List<SpeedToProtectionTest> fetchSpeedToProtection();

	SpeedToProtectionTest insertSpeedToProtection(SpeedToProtectionTest cr);
}
