package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.NaicList;

public interface NaicListService {
   List<NaicList> getNaicList();
} 
