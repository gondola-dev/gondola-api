package com.gondola.app.service;

import java.util.List;

import com.gondola.app.entities.HealthRatings;

public interface HealthRatingsService {
	
	List<HealthRatings> fetchHealthRatingsList();

	HealthRatings insertHealthRatingsList(HealthRatings hr);
}
