package com.gondola.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.StagingUserInfo;

@Repository
public interface StagingUserRepository extends JpaRepository<StagingUserInfo, Integer>{
	
	Optional<StagingUserInfo> findByipaddress(String ipaddress);
}
