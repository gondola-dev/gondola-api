package com.gondola.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.SubscriberList;

@Repository
public interface SubscriberListRepository extends JpaRepository<SubscriberList, Integer>{
	
	Optional<SubscriberList> findByemailaddress(String emailaddress);
	
	List<SubscriberList> findByipaddress(String ipaddress);
}
