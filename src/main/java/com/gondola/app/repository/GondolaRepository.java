package com.gondola.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.QuotesInfo;


@Repository
public interface GondolaRepository extends JpaRepository<QuotesInfo, Long>{
	
	Optional<List<QuotesInfo>> findByipaddress(String ipaddress);

}
