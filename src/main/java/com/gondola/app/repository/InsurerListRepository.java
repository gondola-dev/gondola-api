package com.gondola.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.InsurerList;

@Repository
public interface InsurerListRepository extends JpaRepository<InsurerList, Integer>{

}
