package com.gondola.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.UserInfoList;

@Repository
public interface UserInfoListRepository extends JpaRepository<UserInfoList, Integer>{
	
	Optional<UserInfoList> findByemailaddress(String emailaddress);

}
