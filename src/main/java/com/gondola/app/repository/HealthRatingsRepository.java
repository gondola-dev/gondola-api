package com.gondola.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.HealthRatings;

@Repository
public interface HealthRatingsRepository extends JpaRepository<HealthRatings, Integer>{

}
