package com.gondola.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gondola.app.entities.NaicList;

public interface NaicRepository extends JpaRepository<NaicList, String>{

}
