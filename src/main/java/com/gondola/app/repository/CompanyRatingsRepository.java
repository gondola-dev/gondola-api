package com.gondola.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.CompanyRatings;

@Repository
public interface CompanyRatingsRepository extends JpaRepository<CompanyRatings, Integer>{

}
