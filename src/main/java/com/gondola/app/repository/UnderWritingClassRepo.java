package com.gondola.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gondola.app.entities.UnderWritingClass;

@Repository
public interface UnderWritingClassRepo extends JpaRepository<UnderWritingClass, Integer>{

}
