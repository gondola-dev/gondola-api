package com.gondola.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//import com.gondola.app.entities.SpeedToProtection;
import com.gondola.app.entities.SpeedToProtectionTest;

@Repository
public interface SpeedToProtectionRepository extends JpaRepository<SpeedToProtectionTest, Integer>{

}
